/* -*- mode: C; c-basic-offset: 4 -*- */
#include "g2c_signal.h"
#include "g2c_helpers.h"

#include <config.h>

g2cSignal *
g2c_signal_new(void)
{
    g2cSignal *signal = g_new0(g2cSignal, 1);

    signal->name = NULL;
    signal->handler = NULL;
    signal->object = NULL;
    signal->data = NULL;
    signal->after = FALSE;
    signal->timestamp = NULL;

    return signal;
}

void
g2c_signal_destroy(g2cSignal * signal)
{
    /* Free the strings */
    g_free(signal->name);
    g_free(signal->handler);
    g_free(signal->object);
    g_free(signal->data);
    g_free(signal->timestamp);

    g_free(signal);
}

void
g2c_signal_set_name(g2cSignal * signal, const gchar * name)
{
    g_free(signal->name);
    signal->name = g_strdup(name);
}

void
g2c_signal_set_handler(g2cSignal * signal, const gchar * handler)
{
    g_free(signal->handler);
    signal->handler = g_strdup(handler);
}

void
g2c_signal_set_object(g2cSignal * signal, const gchar * object)
{
    g_free(signal->object);
    signal->object = g_strdup(object);
}

void
g2c_signal_set_data(g2cSignal * signal, const gchar * data)
{
    g_free(signal->data);
    signal->data = g_strdup(data);
}

void
g2c_signal_set_after(g2cSignal * signal, const gchar * after)
{
    signal->after = g2c_get_bool(after);
}

void
g2c_signal_set_timestamp(g2cSignal * signal, const gchar * date)
{
    g_free(signal->timestamp);
    signal->timestamp = g_strdup(date);
}
