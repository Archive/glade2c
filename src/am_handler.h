/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef AM_HANDLER_H
#define AM_HANDLER_H

/* This file manipulates Makefile.am files */

#include <glib.h>
#include <stdio.h>

typedef struct tag_amHandler {
    GList *files;
    FILE *makefile;
    gchar *package;
} amHandler;

amHandler *am_handler_new(const gchar * makefile, const gchar * package);
void am_handler_add_file(const gchar * filename);
void am_handler_destroy(amHandler * handler);

#endif
