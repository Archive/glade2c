/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef G2C_DOC_H
#define G2C_DOC_H

#include <gtk/gtk.h>
#include <stdio.h>
#include <libxml/parser.h>
#include "g2c_project.h"
#include "g2c_file_parser.h"

typedef struct tag_g2cDoc {
    xmlDocPtr xmldoc;
    g2cProject *project;
    xmlNodePtr current;
} g2cDoc;

/******************************************************************
 **                      Global Variables                        **
 ******************************************************************/

FILE *CURRENT_FILE;
gchar *CURRENT_FILE_NAME;
g2cFileParser *CURRENT_SOURCE_PARSER;
g2cFileParser *CURRENT_PO_PARSER;
g2cFileParser *CURRENT_MAKE_PARSER;
g2cFileParser *CURRENT_MAIN_PARSER;


/******************************************************************
 **                    Function Prototypes                       **
 ******************************************************************/

g2cDoc *g2c_doc_new(gchar * xml_file_name);
void g2c_doc_destroy(g2cDoc * doc);

void g2c_doc_parse(g2cDoc * doc);
void g2c_doc_output(g2cDoc * doc);

#endif
