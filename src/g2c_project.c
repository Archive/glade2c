/* -*- mode: C; c-basic-offset: 4 -*- */
#include "g2c_project.h"
#include "g2c_widget.h"
#include "g2c_helpers.h"
#include <string.h>

#include <config.h>

/* Helpers */

static void
free_children(gpointer data, gpointer user_data)
{
    if (NULL != data) {
	g2c_widget_destroy((g2cWidget *) data);

	g_free(data);
    } else
	g_print("Memory deallocation failure in free_children\n");
}


/* Class functions */

g2cProject *
g2c_project_new(void)
{
    g2cProject *project = g_new0(g2cProject, 1);

    allocs = 0;

    project->properties = g_hash_table_new(g_str_hash, g_str_equal);

    project->top_level_widgets = NULL;
    project->name = g_strdup("");
    project->program_name = g_strdup("");
    project->directory = g_strdup(".");
    project->source_directory = g_strdup(".");
    project->pixmaps_directory = g_strdup(".");
    project->gettext_support = FALSE;
    project->gnome_support = FALSE;
    project->gnome_help_support = FALSE;
    project->output_main_file = TRUE;
    project->output_support_files = TRUE;
    project->output_build_files = TRUE;
#ifdef ENABLE_BONOBO
    project->has_bonobo_controls = TRUE;
#endif

    return project;
}

void
g2c_project_destroy(g2cProject * project)
{
    /* Deallocate the properties hash table */
    g_hash_table_foreach(project->properties,
			 g2c_hash_element_free_cb, NULL);

    g_hash_table_destroy(project->properties);

    /* Free the top level widgets */
    g_list_foreach(project->top_level_widgets, free_children, NULL);

    g_list_free(project->top_level_widgets);

    g_free(project->name);
    g_free(project->program_name);
    g_free(project->directory);
    g_free(project->source_directory);
    g_free(project->pixmaps_directory);

    g_free(project);

    if (allocs != 0)
	g_print("%d widgets were not freed\n", allocs);
}

void
g2c_project_set_property(g2cProject * project,
			 const gchar * name, const gchar * value)
{
    if (!strcmp(name, "gettext_support")) {
	project->gettext_support = g2c_get_bool(value);
    } else if (!strcmp(name, "gnome_support")) {
	project->gnome_support = g2c_get_bool(value);
    } else if (!strcmp(name, "gnome_help_support")) {
	project->gnome_help_support = g2c_get_bool(value);
    } else if (!strcmp(name, "name")) {
	project->name = g_strdup(value);
    } else if (!strcmp(name, "program_name")) {
	project->program_name = g_strdup(value);
    } else if (!strcmp(name, "directory")) {
	project->directory = g_strdup(value);
    } else if (!strcmp(name, "source_directory")) {
	project->source_directory = g_strdup(value);
    } else if (!strcmp(name, "pixmaps_directory")) {
	project->pixmaps_directory = g_strdup(value);
    } else if (!strcmp(name, "output_main_file")) {
	project->output_main_file = g2c_get_bool(value);
    } else if (!strcmp(name, "output_support_files")) {
	project->output_support_files = g2c_get_bool(value);
    } else if (!strcmp(name, "output_build_files")) {
	project->output_build_files = g2c_get_bool(value);
    } else {
	g_hash_table_insert(project->properties,
			    g_strdup(name), g_strdup(value));
    }
}

const gchar *
g2c_project_get_property(g2cProject * project, const gchar * name)
{
    return g_hash_table_lookup(project->properties, name);
}

void
g2c_project_add_top_level_widget(g2cProject * project, g2cWidget * widget)
{
    g_assert(project != NULL);
    g_assert(widget != NULL);

    project->top_level_widgets = g_list_append(project->top_level_widgets,
					       widget);
}
