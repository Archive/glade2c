/* -*- mode: C; c-basic-offset: 4 -*- */
#include "am_handler.h"
#include "g2c_helpers.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static gchar *
am_read_line(FILE * file)
{
    /* Reads a line of a file (until a newline) */
    GString *line = g_string_new("");
    gchar *result = NULL;
    gchar buf[1] = { '\0' };

    while (!feof(file)) {
	fread(buf, 1, 1, file);
	if (buf[0] != '\n') {
	    g_string_append_c(line, buf[0]);
	} else
	    break;
    }

    result = g_strdup(line->str);
    g_string_free(line, TRUE);

    return result;
}

amHandler *
am_handler_new(const gchar * makefile, const gchar * package)
{
    /* Create a new Automake handler */
    amHandler *handler = NULL;
    gint i = 0;
    gchar *line = NULL;
    gchar *search_string = NULL;
    gchar *rest_of_line = NULL;
    gchar **tokens = NULL;
    gboolean in_decl = FALSE;

    g_assert(makefile != NULL);

    handler = g_new0(amHandler, 1);
    handler->makefile = fopen(makefile, "r+");

    /* Make sure the file is open */
    g_assert(handler->makefile != NULL);

    handler->package = g_strdup(package);
    search_string = g_strdup_printf("%s_SOURCES", package);

    /* Initialize the GList */
    handler->files = NULL;

    /* Load the names of the files that are in package_SOURCES.
     * Start by finding the package_SOURCES line. */

    while (!feof(handler->makefile)) {
	line = am_read_line(handler->makefile);

	if (in_decl) {
	    tokens = g_strsplit(line, " ", 0);

	    /* Check for non-\ strings. Add them to the list of files */
	    while (tokens[i] != NULL) {
		if ((tokens[i][0] != '\\') && (tokens[i][0] != '=')) {
		    g_list_append(handler->files,
				  g_strdup(g_strstrip(tokens[i])));
		}
	    }
	    g_strfreev(tokens);
	} else {
	    /* Does this line have the package_SOURCES declaration? */
	    if (strstr(line, search_string) != NULL) {
		/* We found it! Grab anything at the end of the line,
		 * up to a "\" character 
		 */
		rest_of_line = strstr(line, search_string);
		rest_of_line =
		    rest_of_line + sizeof(search_string) / sizeof(gchar);
		tokens = g_strsplit(rest_of_line, " ", 0);

		/* Check for non-\ strings. Add them to the list of files */
		while (tokens[i] != NULL) {
		    if ((tokens[i][0] != '\\') && (tokens[i][0] != '=')) {
			g_list_append(handler->files,
				      g_strdup(g_strstrip(tokens[i])));
		    }
		}
		g_strfreev(tokens);

		in_decl = TRUE;
	    }
	}

	g_free(line);
    }

    g_free(search_string);

    return handler;
}

void
am_handler_add_file(const gchar * filename)
{
    /* Add this file to the list of files to be handled in the 
     * package_SOURCES portion of the makefile  */


}

void
am_handler_destroy(amHandler * handler)
{
    /* Close the makefile */
    g_assert(handler->makefile);

    fclose(handler->makefile);

    g_free(handler->package);

    g_list_foreach(handler->files, g2c_list_element_free_cb, NULL);

    g_list_free(handler->files);

    /* Free memory associated with the handler */
    g_free(handler);
}
