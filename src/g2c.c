/* -*- mode: C; c-basic-offset: 4 -*- */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <glib.h>
#include <glib-object.h>

#ifdef USE_GNOME
#include <libgnomeui/libgnomeui.h>
#endif

#include <gtk/gtk.h>
#include "g2c_doc.h"
#include "g2c_helpers.h"

int
main(int argc, char *argv[])
{
    int handler_id;
    g2cDoc *doc = NULL;

    if (argc < 2) {
	g_print("Please provide a .glade file to process\n");
	return 1;
    }

    g_type_init();
    gtk_type_init(0);

#if 0
    handler_id = g_log_set_handler(NULL,
				   G_LOG_LEVEL_MESSAGE,
				   g2c_message_handler, NULL);
#endif

    g_message("Parsing %s\n", argv[1]);

    /* The first parameter is the filename of the glade file */
    doc = g2c_doc_new(argv[1]);
    g_assert(NULL != doc);

    g2c_doc_parse(doc);

    /* Write out to files */
    g2c_doc_output(doc);

    /* Free the document */
    g2c_doc_destroy(doc);

#if 0
    g_log_remove_handler(NULL, handler_id);
#endif

    return 0;
}
