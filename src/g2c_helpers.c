/* -*- mode: C; c-basic-offset: 4 -*- */
#include "g2c_helpers.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <gtk/gtk.h>

#include <config.h>

gchar *
g2c_stringify(const gchar * pstr)
{
    /* Take this string and wrap it in a _("%s") if
     * project->gettext_support is true. Otherwise use "%s".
     */
    gchar *result = NULL;

    if (pstr != NULL) {
	if (CURRENT_PROJECT->gettext_support) {
	    result = g_strdup_printf("_(\"%s\")", pstr);
	} else {
	    result = g_strdup_printf("\"%s\"", pstr);
	}
    } else
	result = g_strdup("\"\"");

    return result;		// Freed by caller
}


void
g2c_hash_element_free_cb(gpointer key, gpointer value, gpointer user_data)
{
    g_free(key);
    g_free(value);
}

void
g2c_list_element_free_cb(gpointer data, gpointer user_data)
{
    g_free(data);
}

gint
g2c_string_list_compare_cb(gconstpointer a, gconstpointer b)
{
    return strcmp((char *) a, (char *) b);
}

void
g2c_message_handler(const gchar * log_domain,
		    GLogLevelFlags log_level,
		    const gchar * message, gpointer user_data)
{
    /* printf( message ); */
}

gchar *
g2c_transform_name(gchar * name, NameTransform transform)
{
    gchar *result = NULL;
    gchar **parts = NULL;
    gint i = 0;
    GString *s_result = NULL;
    gchar next = 0;

    switch (transform) {
    case NT_GUI:
	result = g_strconcat(name, "_gui.h", NULL);
	g_strup(result);
	result = g_strdelimit(result, ".", '_');
	break;
    case NT_STANDARD:
	result = g_strconcat(name, ".h", NULL);
	g_strup(result);
	result = g_strdelimit(result, ".", '_');
	break;
    case NT_TYPENAME:
	/* This one is a little trickier.  We want to remove underscores, then
	 * capitalize the letters that would come after the underscores, making
	 * a proper type name.
	 * 
	 * Example:  my_new_widget ==> MyNewWidget
	 */
	parts = g_strsplit(name, "_", 0);

	/* The first letter of each string should be capitalized */

	while (parts[i] != NULL) {
	    parts[i][0] = toupper(parts[i][0]);
	    i++;
	}

	/* Put them all together */
	result = g_strjoinv(NULL, parts);

	/* Clean up */
	g_strfreev(parts);

	break;
    case NT_FUNCTION:
	/* When making gtk calls, you'll need to take a type name,
	 * say GtkWidget, and call gtk_widget_whatever.  The input 
	 * variable "name" is the type name for the widget.
	 * This will transform "GtkWidget" to "gtk_widget".
	 */
	s_result = g_string_new("");
	i = 0;

	while (name[i] != 0) {
	    if ((name[i] >= 'A') && (name[i] <= 'Z')) {
		next = tolower(name[i]);
		if (i != 0)
		    g_string_append_c(s_result, '_');
		g_string_append_c(s_result, next);
	    } else {
		g_string_append_c(s_result, name[i]);
	    }

	    i++;
	}

	result = g_strdup(s_result->str);
	g_string_free(s_result, TRUE);
    }

    return result;		/* Freed by caller */
}

gboolean
g2c_get_bool(const gchar * arg_value)
{
    if (arg_value == NULL)
	return FALSE;
    else if (!g_strcasecmp(arg_value, "TRUE") ||
	     !g_strcasecmp(arg_value, "yes"))
	return TRUE;
    else
	return FALSE;
}

gchar *
g2c_get_bool_s(const gchar * arg_value)
{
    gchar *result = NULL;

    if (arg_value == NULL)
	result = g_strdup("0");
    else if (!g_strcasecmp(arg_value, "TRUE") ||
	     !g_strcasecmp(arg_value, "yes"))
	result = g_strdup("TRUE");
    else if (!g_strcasecmp(arg_value, "FALSE") ||
	     !g_strcasecmp(arg_value, "no"))
	result = g_strdup("FALSE");
    else
	result = g_strdup(arg_value);

    return result;
}

gchar *
g2c_format_argument(const gchar * arg_type_name,
		    const gchar * arg_name, const gchar * arg_value)
{
    /* Using the type name, get the type of the argument, and
     * format the corresponding value based on the type.
     * For example, if the base type is GtkString, then the 
     * argument should be in quotes: "argument".  If the base
     * type is not a string, it should not have quotes: GTK_WINDOW_POPUP.
     * Null string values should be listed as "", and other null
     * values should return 0 or NULL (if a pointer type).
     * If the parameter does not have a known type, just use the value given.
     */

    gchar *result = NULL;
    GType gtype;
    GObjectClass *oclass;
    GParamSpec *pspec;


    if (arg_name == NULL) {
	return NULL;
    }

    /* Get the GType, and make sure the class is loaded */
    gtype = g_type_from_name(arg_type_name);
    oclass = g_type_class_ref(gtype);

    pspec = g_object_class_find_property(oclass, arg_name);
    if (pspec != NULL) {	/* all went ok */
	switch (G_TYPE_FUNDAMENTAL(pspec->value_type)) {
	case G_TYPE_BOOLEAN:
	    if (arg_value == NULL) {
		g_warning("Null value for arg: %s", arg_name);
		result = g_strdup("FALSE");
	    } else {
		return g2c_get_bool_s(arg_value);
	    }
	    break;
	case G_TYPE_STRING:
	    if (arg_value == NULL)
		result = g2c_stringify("NULL");
	    else
		result = g2c_stringify(arg_value);
	    break;
	case G_TYPE_POINTER:
	    if (arg_value == NULL)
		result = g_strdup("NULL");
	    else
		result = g_strdup(arg_value);
	default:
	    if (arg_value == NULL)
		result = g_strdup("0");
	    else
		result = g_strdup(arg_value);
	}
    } else {
	g_warning("could not look up pspec for %s", arg_name);
	result = g2c_get_bool_s(arg_value);
    }

    g_type_class_unref(oclass);

    return result;
}

gboolean
g2c_check_file_exists(gchar * filename)
{
    return g_file_test(filename, G_FILE_TEST_IS_REGULAR);
}
