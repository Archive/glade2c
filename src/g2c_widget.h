/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef G2C_WIDGET_H
#define G2C_WIDGET_H

#include <stdio.h>
#include <gtk/gtk.h>
#include "g2c_signal.h"
#include "g2c_accel.h"

#define MAX_CREATE_PARAMS 10

int allocs;

typedef enum {
    BOX_PACKING,
    TABLE_PACKING,
    PACKER_PACKING
} g2cPackType;

typedef struct tag_g2cBoxPacking {
    gboolean expand;
    gboolean fill;
    guint padding;
} g2cBoxPacking;

typedef struct tag_g2cPackerPacking {
    gchar *side;
    gchar *anchor;
    gboolean expand;
    guint xpad;
    guint ypad;
    guint xipad;
    guint yipad;
    guint border_width;
    gboolean use_default;
    gboolean xfill;
    gboolean yfill;
} g2cPackerPacking;

typedef struct tag_g2cTablePacking {
    guint left_attach;
    guint right_attach;
    guint top_attach;
    guint bottom_attach;
    guint xpad;
    guint ypad;
    gboolean xexpand;
    gboolean yexpand;
    gboolean xshrink;
    gboolean yshrink;
    gboolean xfill;
    gboolean yfill;
} g2cTablePacking;

typedef union {
    g2cBoxPacking box;
    g2cTablePacking table;
    g2cPackerPacking packer;
} g2cPacking;

typedef struct tag_g2cWidget {
    GHashTable *properties;
    GList *children;
    GList *signals;
    GList *accelerators;
    GList *radio_groups;
    struct tag_g2cWidget *parent;
    GType klass;
    gchar *klass_name;
    gchar *name;
    g2cPackType packing_type;
    g2cPacking packing;
    guint order;
    gchar *internal_child;
    gboolean is_bonobo_control;
} g2cWidget;


typedef void (*g2cHandlerFunc) (g2cWidget * widget);
typedef void (*g2cCreateFunc) (g2cWidget * widget);

typedef struct tag_g2cCreateFunction {
    gchar *widget_class;
    gchar *create_function;
    gchar *parameters[MAX_CREATE_PARAMS];
    g2cCreateFunc handler;
} g2cCreateFunction;

typedef struct tag_g2cAfterParam {
    gchar *type;
    gchar *param;
    gchar *params[MAX_CREATE_PARAMS];
    gchar *format;
    g2cHandlerFunc handler;
} g2cAfterParam;

typedef struct tag_g2cIgnoreParam {
    gchar *type;
    gchar *param;
} g2cIgnoreParam;

typedef struct tag_g2cRemapParam {
    gchar *type;
    gchar *old_param;
    gchar *new_param;
} g2cRemapParam;

typedef struct tag_g2cSpecialHandler {
    gchar *type;
    gchar *keyword;
    gchar *format;
    gchar *params[MAX_CREATE_PARAMS];
    gchar *conditional;
    g2cHandlerFunc handler;
} g2cSpecialHandler;

/* static declarations */
void
 add_to_signal_list(gchar * signal_name);

void
 clear_signal_list(void);

gboolean is_in_signal_list(gchar * signal_name);

/* Class functions */

g2cWidget *g2c_widget_new(const gchar * base);

void
 g2c_widget_destroy(g2cWidget * widget);

g2cWidget *g2c_widget_get_top_parent(g2cWidget * widget);

void
 g2c_widget_set_order(g2cWidget * widget, gint order);

void
 g2c_widget_set_property(g2cWidget * widget,
			 const gchar * name, const gchar * value);

gchar *g2c_widget_get_property(g2cWidget * widget, const gchar * name);

void
 g2c_widget_add_subwidget(g2cWidget * widget, g2cWidget * subwidget);

void
 g2c_widget_add_signal(g2cWidget * widget, g2cSignal * signal);

void
 g2c_widget_add_accel(g2cWidget * widget, g2cAccel * accel);

void
 g2c_widget_add_focus_target(g2cWidget * widget,
			     const gchar * focus_target);

void
 g2c_widget_add_radio_group(g2cWidget * widget, const gchar * group_name);

void
 g2c_widget_add_option_menu(g2cWidget * widget);

void
 g2c_widget_create_signal_connect_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_signal_handler_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_signal_prototype_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_struct_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_accel_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_temp_declaration_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_accel_declaration_cb(gpointer data, gpointer user_data);

void
 g2c_widget_create_focus_accel_declaration_cb(gpointer data,
					      gpointer user_data);

void
 g2c_widget_create_arg_cb(gpointer key, gpointer data, gpointer user_data);

gchar *g2c_widget_create_string(g2cWidget * widget);

void
 g2c_widget_output_after_children_cb(gpointer key,
				     gpointer data, gpointer user_data);

g2cWidget *g2c_widget_find_by_name(g2cWidget * parent_widget,
				   const gchar * widget_name);

g2cWidget *g2c_widget_find_by_type_name(g2cWidget * parent_widget,
					const gchar * type_name);

g2cWidget *g2c_widget_find_by_type(g2cWidget * parent_widget, GType type);

/* find non internal_child parent widget */
g2cWidget *g2c_widget_find_non_internal_parent(g2cWidget *widget);

gboolean g2c_widget_ignore_param(g2cWidget * widget, gchar * param);

gchar *g2c_widget_remap_param(g2cWidget * widget, gchar * param);

gboolean g2c_widget_special_handler(g2cWidget * widget, gchar * keyword);

#endif
