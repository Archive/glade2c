/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef G2C_HELPERS_H
#define G2C_HELPERS_H

#include <glib.h>
#include <string.h>
#include "g2c_project.h"

/* Helper types/enums */

typedef enum tag_NameTransform {
    NT_GUI,
    NT_STANDARD,
    NT_TYPENAME,
    NT_FUNCTION
} NameTransform;

gchar *g2c_stringify(const gchar * pstr);

void g2c_hash_element_free_cb(gpointer key,
			      gpointer value, gpointer user_data);

void g2c_list_element_free_cb(gpointer data, gpointer user_data);

gint g2c_string_list_compare_cb(gconstpointer a, gconstpointer b);

void g2c_message_handler(const gchar * log_domain,
			 GLogLevelFlags log_level,
			 const gchar * message, gpointer user_data);

gchar *g2c_transform_name(gchar * name, NameTransform transform);

gchar *g2c_format_argument(const gchar * arg_type_name,
			   const gchar * arg_name,
			   const gchar * arg_value);

gboolean g2c_get_bool(const gchar * arg_value);

gchar *g2c_get_bool_s(const gchar * arg_value);

gboolean g2c_check_file_exists(gchar * filename);

#endif
