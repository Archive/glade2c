/* -*- mode: C; c-basic-offset: 4 -*- */
#include <string.h>
#include <stdio.h>
#include "g2c_widget.h"
#include "g2c_signal.h"
#include "g2c_helpers.h"
#include "g2c_doc.h"
#include "g2c_signals.h"

#include <config.h>

#ifdef USE_GNOME
#include <gnome.h>
#endif

/* This list contains a signal handler names that have already been added */
static GList *signal_list = NULL;

/* Special handler prototypes */
void bin_label_handler(g2cWidget * widget);
void calendar_set_display_options(g2cWidget * widget);
void clist_show_titles_handler(g2cWidget * widget);
void clist_set_column_widths_handler(g2cWidget * widget);
void combo_items_handler(g2cWidget * widget);
void curve_set_range_handler(g2cWidget * widget);
void file_selection_fileops_handler(g2cWidget * widget);
void option_menu_initial_choice_handler(g2cWidget * widget);

/* Creation handler prototypes */
void create_custom_widget(g2cWidget * widget);
void create_gnome_app(g2cWidget * widget);
void create_gnome_date_edit(g2cWidget * widget);
void create_gnome_dock(g2cWidget * widget);
void create_gnome_dock_item(g2cWidget * widget);
void create_gnome_entry(g2cWidget * widget);
void create_gnome_file_entry(g2cWidget * widget);
void create_gnome_icon_entry(g2cWidget * widget);
void create_gnome_icon_list(g2cWidget * widget);
void create_gnome_number_entry(g2cWidget * widget);
void create_gnome_pixmap_entry(g2cWidget * widget);
void gnome_druid_page_start_logo_image_handler(g2cWidget * widget);
void gnome_druid_page_start_watermark_image_handler(g2cWidget * widget);
void gnome_druid_page_standard_logo_image_handler(g2cWidget * widget);
void gnome_druid_page_finish_logo_image_handler(g2cWidget * widget);
void gnome_druid_page_finish_watermark_image_handler(g2cWidget * widget);
void create_gtk_aspect_frame(g2cWidget * widget);
void create_gtk_button(g2cWidget * widget);
void create_gtk_pixmap(g2cWidget * widget);
void create_gtk_entry(g2cWidget * widget);
void create_gtk_hscale(g2cWidget * widget);
void create_gtk_spin_button(g2cWidget * widget);
void create_gtk_vbox(g2cWidget * widget);
void create_gtk_vscale(g2cWidget * widget);

/* Other functions */
guint position_after_last_brace(FILE * file);
gchar *process_color(gchar * color);

/* Table of create functions */
static g2cCreateFunction create_functions[] = {
    {"Custom", NULL,
     {NULL},
     create_custom_widget},

    {"GnomeApp", NULL,
     {NULL},
     create_gnome_app},

    {"GnomeAppBar", "gnome_appbar_new (%s, %s, GNOME_PREFERENCES_USER)",
     {"has_progress", "has_status", NULL},
     NULL},

    {"GnomeDateEdit", NULL,
     {NULL},
     create_gnome_date_edit},

    {"GnomeDock", NULL,
     {NULL},
     create_gnome_dock},

    {"GnomeDockItem", NULL,
     {NULL},
     create_gnome_dock_item},

    {"GnomeEntry", NULL,
     {NULL},
     create_gnome_entry},

    {"GnomeFileEntry", NULL,
     {NULL},
     create_gnome_file_entry},

    {"GnomeIconEntry", NULL,
     {NULL},
     create_gnome_icon_entry},

    {"GnomeIconList", NULL,
     {NULL},
     create_gnome_icon_list},

    {"GnomeHRef", "gnome_href_new (_(\"%s\"), %s)",
     {"url", "label", NULL},
     NULL},

    {"GnomeNumberEntry", NULL,
     {NULL},
     create_gnome_number_entry},

    {"GnomePixmapEntry", NULL,
     {NULL},
     create_gnome_pixmap_entry},


    {"GtkAccelLabel", "gtk_accel_label_new (%s)",
     {"label", NULL},
     NULL},

    {"GtkArrow", "gtk_arrow_new (%s, %s)",
     {"arrow_type", "shadow_type", NULL},
     NULL},

    {"GtkAlignment", "gtk_alignment_new (%s, %s, %s, %s)",
     {"xalign", "yalign", "xscale", "yscale", NULL},
     NULL},

    {"GtkAspectFrame", NULL,
     {NULL},
     create_gtk_aspect_frame},

    {"GtkButton", NULL,
     {NULL},
     create_gtk_button},

    {"GtkCheckButton", "gtk_check_button_new_with_label (\"\")",
     {NULL},
     NULL},

    {"GtkCheckMenuItem", "gtk_check_menu_item_new_with_label (\"\")",
     {NULL},
     NULL},

    {"GtkCList", "gtk_clist_new (%s)",
     {"columns", NULL},
     NULL},

    {"GtkClock", "gtk_clock_new (%s)",
     {"type", NULL},
     NULL},

    {"GtkColorSelectionDialog", "gtk_color_selection_dialog_new (%s)",
     {"title", NULL},
     NULL},

    {"GtkCTree", "gtk_ctree_new (%s, %s)",
     {"columns", "tree_column", NULL},
     NULL},

    {"GtkDial",
     "gtk_dial_new (GTK_ADJUSTMENT (gtk_adjustment_new (%s, %s, %s, %s, %s, %s)))",
     {"value", "lower", "upper", "step", "page", "page_size", NULL},
     NULL},

    {"GtkEntry", NULL,
     {NULL},
     create_gtk_entry},

    {"GtkFileSelection", "gtk_file_selection_new (%s)",
     {"title", NULL},
     NULL},

    {"GtkFontSelectionDialog", "gtk_font_selection_dialog_new (%s)",
     {"title", NULL},
     NULL},

    {"GtkFrame", "gtk_frame_new (%s)",
     {"label", NULL},
     NULL},

    {"GtkHBox", "gtk_hbox_new (%s, %s)",
     {"homogeneous", "spacing", NULL},
     NULL},

    {"GtkHButtonBox", "gtk_hbutton_box_new ()",
     {NULL},
     NULL},

    {"GtkHPaned", "gtk_hpaned_new ()",
     {NULL},
     NULL},

    {"GtkHRuler", "gtk_hruler_new ()",
     {NULL},
     NULL},

    {"GtkHScale", NULL,
     {NULL},
     create_gtk_hscale},

    {"GtkImage",
     "gtk_image_new (gdk_image_new (%s, gdk_visual_get_best (), %s, %s), NULL)",
     {"image_type", "image_width", "image_height", NULL},
     NULL},

    {"GtkLabel", "gtk_label_new (_(%s))",
     {"label", NULL},
     NULL},

    {"GtkLayout", "gtk_layout_new (NULL,NULL)",
     {NULL},
     NULL},

    {"GtkMenuItem", "gtk_menu_item_new_with_label (_(\"%s\"))",
     {"label", NULL},
     NULL},

    {"GtkPixmap", NULL,
     {NULL},
     create_gtk_pixmap},

    {"GtkPreview", "gtk_preview_new (%s)",
     {"type", NULL},
     NULL},

    {"GtkRadioButton", "gtk_radio_button_new_with_label (gui->%s, \"\")",
     {"group", NULL},
     NULL},

    {"GtkRadioMenuItem",
     "gtk_radio_menu_item_new_with_label (gui->%s, \"\")",
     {"group", NULL},
     NULL},

    {"GtkScrolledWindow", "gtk_scrolled_window_new (%s, %s)",
     {"hupdate_policy", "vupdate_policy", NULL},
     NULL},

    {"GtkSpinButton", NULL,
     {NULL},
     create_gtk_spin_button},

    {"GtkTable", "gtk_table_new (%s, %s, %s)",
     {"rows", "columns", "homogeneous", NULL},
     NULL},

    {"GtkText", "gtk_text_new (NULL, NULL)",
     {NULL},
     NULL},

    {"GtkToolbar", "gtk_toolbar_new ()",
     {NULL},
     NULL},

    {"GtkVBox", NULL,
     {NULL},
     create_gtk_vbox},

    {"GtkVButtonBox", "gtk_vbutton_box_new ()",
     {NULL},
     NULL},

    {"GtkVPaned", "gtk_vpaned_new ()",
     {NULL},
     NULL},

    {"GtkViewport", "gtk_viewport_new (%s, %s)",
     {"hadjustment", "vadjustment", NULL},
     NULL},

    {"GtkVRuler", "gtk_vruler_new ()",
     {NULL},
     NULL},

    {"GtkVScale", NULL,
     {NULL},
     create_gtk_vscale},

    {"GtkWindow", "gtk_window_new (%s)",
     {"type", NULL},
     NULL},

    {NULL, NULL,
     {NULL},
     NULL}
};

static g2cAfterParam after_params[] = {
    {"GtkOptionMenu", "initial_choice",
     {NULL},
     NULL,
     option_menu_initial_choice_handler},

    {NULL, NULL,
     {NULL},
     NULL,
     NULL}
};

/* Table of parameters to ignore */
static g2cIgnoreParam ignore_params[] = {
    {"Custom", "creation_function"},
    {"Custom", "string1"},
    {"Custom", "string2"},
    {"Custom", "int1"},
    {"Custom", "int2"},
    {"Custom", "last_modification_time"},
    {"GnomeAppBar", "has_progress"},
    {"GnomeAppBar", "has_status"},
    {"GnomeDockItem", "band"},
    {"GnomeDateEdit", "show_time"},
    {"GnomeDateEdit", "week_start_monday"},
    {"GnomeDateEdit", "use_24_format"},
    {"GnomeDateEdit", "upper_hour"},
    {"GnomeDockItem", "exclusive"},
    {"GnomeDockItem", "offset"},
    {"GnomeDockItem", "placement"},
    {"GnomeDockItem", "never_horizontal"},
    {"GnomeDockItem", "never_vertical"},
    {"GnomeDockItem", "never_floating"},
    {"GnomeDockItem", "position"},
    {"GnomeDockItem", "locked"},
    {"GnomeEntry", "history_id"},
    {"GnomeFileEntry", "title"},
    {"GnomeFileEntry", "history_id"},
    {"GnomeFontPicker", "use_font_size"},
    {"GnomeIconEntry", "title"},
    {"GnomeIconEntry", "history_id"},
    {"GnomeIconList", "text_editable"},
    {"GnomeIconList", "text_static"},
    {"GnomeIconList", "icon_width"},
    {"GnomeHRef", "label"},
    {"GnomeHRef", "url"},
    {"GnomeNumberEntry", "title"},
    {"GnomeNumberEntry", "history_id"},
    {"GnomePixmapEntry", "title"},
    {"GnomePixmapEntry", "history_id"},
    {"GnomePixmapEntry", "preview"},

    {"GtkAccelLabel", "label"},
    {"GtkAlignment", "xalign"},
    {"GtkAlignment", "yalign"},
    {"GtkAlignment", "xscale"},
    {"GtkAlignment", "yscale"},
    {"GtkAspectFrame", "label"},
    {"GtkAspectFrame", "xalign"},
    {"GtkAspectFrame", "yalign"},
    {"GtkAspectFrame", "ratio"},
    {"GtkAspectFrame", "obey_child"},
    {"GtkWidget", "_tab_label"},
    {"GtkArrow", "arrow_type"},
    {"GtkArrow", "shadow_type"},
    {"GtkButton", "icon"},
    {"GtkButton", "stock_button"},
    {"GtkButton", "stock_pixmap"},
    {"GtkButtonBox", "child_min_height"},
    {"GtkButtonBox", "child_ipad_y"},
    {"GtkCalendar", "show_day_names"},
    {"GtkCalendar", "no_month_change"},
    {"GtkCalendar", "show_week_numbers"},
    {"GtkCalendar", "week_start_monday"},
    {"GtkCList", "columns"},
    {"GtkClock", "type"},
    {"GtkCombo", "ok_if_empty"},
    {"GtkCTree", "tree_column"},
    {"GtkCurve", "min_x"},
    {"GtkCurve", "min_y"},
    {"GtkCurve", "max_y"},
    {"GtkDial", "value"},
    {"GtkDial", "lower"},
    {"GtkDial", "upper"},
    {"GtkDial", "step"},
    {"GtkDial", "page"},
    {"GtkDial", "page_size"},
    {"GtkFrame", "label"},
    {"GtkGammaCurve", "min_x"},
    {"GtkGammaCurve", "min_y"},
    {"GtkGammaCurve", "max_y"},
    {"GtkHBox", "homogeneous"},
    {"GtkHBox", "spacing"},
    {"GtkImage", "image_visual"},
    {"GtkImage", "image_type"},
    {"GtkImage", "image_width"},
    {"GtkImage", "image_height"},
    {"GtkRuler", "upper"},
    {"GtkRuler", "position"},
    {"GtkRuler", "lower"},
    {"GtkHScale", "value"},
    {"GtkHScale", "lower"},
    {"GtkHScale", "upper"},
    {"GtkHScale", "step"},
    {"GtkHScale", "page"},
    {"GtkHScale", "page_size"},
    {"GtkLabel", "focus_target"},
    {"GtkLayout", "area_height"},
    {"GtkMenuItem", "stock_item"},
    {"GtkMenuItem", "stock_icon"},
    {"GtkMenuItem", "icon"},
    {"GtkNotebook", "label"},
    {"GtkOptionMenu", "items"},
    {"GtkPixmap", "filename"},
    {"GtkPreview", "type"},
    {"GtkProgress", "lower"},
    {"GtkProgress", "upper"},
    {"GtkRadioButton", "group"},
    {"GtkRadioMenuItem", "group"},
    {"GtkScrolledWindow", "hupdate_policy"},
    {"GtkScrolledWindow", "vupdate_policy"},
    {"GtkSpinButton", "value"},
    {"GtkSpinButton", "lower"},
    {"GtkSpinButton", "upper"},
    {"GtkSpinButton", "page"},
    {"GtkSpinButton", "page_size"},
    {"GtkSpinButton", "step"},
    {"GtkSpinButton", "climb_rate"},
    {"GtkSpinButton", "digits"},
    {"GtkToolbar", "orientation"},
    {"GtkToolbar", "type"},
    {"GtkVBox", "homogeneous"},
    {"GtkVBox", "spacing"},
    {"GtkViewport", "hadjustment"},
    {"GtkVScale", "value"},
    {"GtkVScale", "lower"},
    {"GtkVScale", "upper"},
    {"GtkVScale", "step"},
    {"GtkVScale", "page"},
    {"GtkVScale", "page_size"},
    {"GtkViewport", "vadjustment"},
    {"GtkWidget", "internal_child"},
    {"GtkWidget", "new_group"},
    {"GtkWidget", "value"},
    {"GtkWindow", "type"},
    {NULL, NULL}
};

/* Some parameters have arguments that are named differently than Glade XML */
static g2cRemapParam remap_params[] = {
    {"GnomeDockItem", "shadow_type", "shadow"},
    {"GtkArrow", "shadow_type", "shadow_type"},
    {"GtkWindow", "position", "window_position"},
    {"GtkEntry", "text_max_length", "max_length"},
    {"GtkEntry", "text_visible", "visibility"},
    {"GtkFrame", "shadow_type", "shadow"},
    {"GtkHandleBox", "shadow_type", "shadow"},
    {"GtkNotebook", "popup_enable", "enable_popup"},
    {"GtkRange", "policy", "update_policy"},
    {"GtkTable", "rows", "n_rows"},
    {"GtkTable", "columns", "n_columns"},
    {NULL, NULL, NULL}
};

/* Handle arguments that do not have Gtk arg equivalents
 * Format: { type, keyword, format, { params }, conditional, handler }
 */
static g2cSpecialHandler special_handlers[] = {
    {"GnomeApp", "enable_layout_config",
     "\tgnome_app_enable_layout_config (GNOME_APP (gui->%s), %s);\n",
     {"name", "enable_layout_config", NULL},
     NULL,
     NULL},

    {"GnomeColorPicker", "title",
     "\tgnome_color_picker_set_title (GNOME_COLOR_PICKER (gui->%s), _(\"%s\"));\n",
     {"name", "title", NULL},
     NULL,
     NULL},

    {"GnomeColorPicker", "use_alpha",
     "\tgnome_color_picker_set_use_alpha (GNOME_COLOR_PICKER (gui->%s), %s);\n",
     {"name", "use_alpha", NULL},
     NULL,
     NULL},

    {"GnomeColorPicker", "dither",
     "\tgnome_color_picker_set_dither (GNOME_COLOR_PICKER (gui->%s), %s);\n",
     {"name", "dither", NULL},
     NULL,
     NULL},

    {"GnomeDateEdit", "lower_hour",
     "\tgnome_date_edit_set_popup_range (GNOME_DATE_EDIT (gui->%s), %s, %s);\n",
     {"name", "lower_hour", "upper_hour", NULL},
     NULL,
     NULL},

    {"GnomeDialog", "hide_on_close",
     "\tgnome_dialog_close_hides (GNOME_DIALOG (gui->%s), %s);\n",
     {"name", "hide_on_close", NULL},
     NULL,
     NULL},

    {"GnomeDialog", "auto_close",
     "\tgnome_dialog_set_close (GNOME_DIALOG (gui->%s), %s);\n",
     {"name", "auto_close", NULL},
     NULL,
     NULL},

    {"GnomeDock", "allow_floating",
     "\tgnome_dock_allow_floating_items (GNOME_DOCK (gui->%s), %s);\n",
     {"name", "allow_floating", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "title",
     "\tgnome_druid_page_start_set_title (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                  _(\"%s\"));\n",
     {"name", "title", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "text",
     "\tgnome_druid_page_start_set_text (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                 _(\"%s\"));\n",
     {"name", "text", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "logo_image",
     NULL,
     {NULL},
     NULL,
     gnome_druid_page_start_logo_image_handler},

    {"GnomeDruidPageStart", "watermark_image",
     NULL,
     {NULL},
     NULL,
     gnome_druid_page_start_watermark_image_handler},

    {"GnomeDruidPageStart", "background_color",
     "\tgnome_druid_page_start_set_bg_color (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                      &%s_bg_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "title_color",
     "\tgnome_druid_page_start_set_title_color (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                         &%s_title_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "logo_background_color",
     "\tgnome_druid_page_start_set_logo_bg_color (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                           &%s_logo_bg_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "text_color",
     "\tgnome_druid_page_start_set_text_color (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                        &%s_text_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStart", "textbox_color",
     "\tgnome_druid_page_start_set_textbox_color (GNOME_DRUID_PAGE_START (gui->%s), \n"
     "\t                                           &%s_textbox_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStandard", "logo_image",
     NULL,
     {NULL},
     NULL,
     gnome_druid_page_standard_logo_image_handler},

    {"GnomeDruidPageStandard", "title",
     "\tgnome_druid_page_standard_set_title (GNOME_DRUID_PAGE_STANDARD (gui->%s), \n"
     "\t                                     _(\"%s\"));\n",
     {"name", "title", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStandard", "text",
     "\tgnome_druid_page_standard_set_text (GNOME_DRUID_PAGE_STANDARD (gui->%s), \n"
     "\t                                    _(\"%s\"));\n",
     {"name", "text", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStandard", "background_color",
     "\tgnome_druid_page_standard_set_bg_color (GNOME_DRUID_PAGE_STANDARD (gui->%s), \n"
     "\t                                         &%s_bg_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStandard", "title_color",
     "\tgnome_druid_page_standard_set_title_color (GNOME_DRUID_PAGE_STANDARD (gui->%s), \n"
     "\t                                            &%s_title_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageStandard", "logo_background_color",
     "\tgnome_druid_page_standard_set_logo_bg_color (GNOME_DRUID_PAGE_STANDARD (gui->%s), \n"
     "\t                                              &%s_logo_bg_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "logo_image",
     NULL,
     {NULL},
     NULL,
     gnome_druid_page_finish_logo_image_handler},

    {"GnomeDruidPageFinish", "watermark_image",
     NULL,
     {NULL},
     NULL,
     gnome_druid_page_finish_watermark_image_handler},

    {"GnomeDruidPageFinish", "title",
     "\tgnome_druid_page_finish_set_title (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                   _(\"%s\"));\n",
     {"name", "title", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "text",
     "\tgnome_druid_page_finish_set_text (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                  _(\"%s\"));\n",
     {"name", "text", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "background_color",
     "\tgnome_druid_page_finish_set_bg_color (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                       &%s_bg_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "title_color",
     "\tgnome_druid_page_finish_set_title_color (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                          &%s_title_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "logo_background_color",
     "\tgnome_druid_page_finish_set_logo_bg_color (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                            &%s_logo_bg_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "text_color",
     "\tgnome_druid_page_finish_set_text_color (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                         &%s_text_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeDruidPageFinish", "textbox_color",
     "\tgnome_druid_page_finish_set_textbox_color (GNOME_DRUID_PAGE_FINISH (gui->%s), \n"
     "\t                                            &%s_textbox_color);\n",
     {"name", "name", NULL},
     NULL,
     NULL},

    {"GnomeEntry", "max_saved",
     "\tgnome_entry_set_max_saved (GNOME_ENTRY (gui->%s), %s);\n",
     {"name", "max_saved", NULL},
     NULL,
     NULL},

    {"GnomeFileEntry", "max_saved",
     "\tgnome_entry_set_max_saved (GNOME_ENTRY (GNOME_FILE_ENTRY (gui->%s)->gentry), %s);\n",
     {"name", "max_saved", NULL},
     NULL,
     NULL},

    {"GnomeFileEntry", "directory",
     "\tgnome_file_entry_set_directory ( GNOME_FILE_ENTRY (gui->%s), %s);\n",
     {"name", "directory", NULL},
     NULL,
     NULL},

    {"GnomeFileEntry", "modal",
     "\tgnome_file_entry_set_modal ( GNOME_FILE_ENTRY (gui->%s), %s);\n",
     {"name", "modal", NULL},
     NULL,
     NULL},

    {"GnomeFontPicker", "title",
     "\tgnome_font_picker_set_title (GNOME_FONT_PICKER (gui->%s), %s);\n",
     {"name", "$title", NULL},
     NULL,
     NULL},

    {"GnomeFontPicker", "preview_text",
     "\tgnome_font_picker_set_preview_text (GNOME_FONT_PICKER (gui->%s), %s);\n",
     {"name", "$preview_text", NULL},
     NULL,
     NULL},

    {"GnomeFontPicker", "mode",
     "\tgnome_font_picker_set_mode (GNOME_FONT_PICKER (gui->%s), %s);\n",
     {"name", "mode", NULL},
     NULL,
     NULL},

    {"GnomeFontPicker", "show_size",
     "\tgnome_font_picker_fi_set_show_size (GNOME_FONT_PICKER (gui->%s), %s);\n",
     {"name", "show_size", NULL},
     NULL,
     NULL},

    {"GnomeFontPicker", "use_font",
     "\tgnome_font_picker_fi_set_use_font_in_label (GNOME_FONT_PICKER (gui->%s), %s, %s);\n",
     {"name", "use_font", "use_font_size", NULL},
     NULL,
     NULL},

    {"GnomeIconEntry", "max_saved",
     "\tgnome_entry_set_max_saved (GNOME_ENTRY (GNOME_FILE_ENTRY (GNOME_ICON_ENTRY (gui->%s)->fentry)->gentry), %s);\n",
     {"name", "max_saved", NULL},
     NULL,
     NULL},

    {"GnomeIconList", "row_spacing",
     "\tgnome_icon_list_set_row_spacing (GNOME_ICON_LIST (gui->%s), %s);\n",
     {"name", "row_spacing", NULL},
     NULL,
     NULL},

    {"GnomeIconList", "selection_mode",
     "\tgnome_icon_list_set_selection_mode (GNOME_ICON_LIST (gui->%s), %s);\n",
     {"name", "selection_mode", NULL},
     NULL,
     NULL},

    {"GnomeIconList", "text_spacing",
     "\tgnome_icon_list_set_text_spacing (GNOME_ICON_LIST (gui->%s), %s);\n",
     {"name", "text_spacing", NULL},
     NULL,
     NULL},

    {"GnomeIconList", "column_spacing",
     "\tgnome_icon_list_set_col_spacing (GNOME_ICON_LIST (gui->%s), %s);\n",
     {"name", "column_spacing", NULL},
     NULL,
     NULL},

    {"GnomeLess", "font",
     "\tgnome_less_set_font (GNOME_LESS (gui->%s), gdk_font_load (\"%s\"));\n",
     {"name", "font", NULL},
     NULL,
     NULL},

    {"GnomeNumberEntry", "max_saved",
     "\tgnome_entry_set_max_saved (GNOME_ENTRY (GNOME_NUMBER_ENTRY (gui->%s)->gentry), %s);\n",
     {"name", "max_saved", NULL},
     NULL,
     NULL},

    {"GnomeNumberEntry", "title",
     "\tgnome_number_entry_set_title (GNOME_NUMBER_ENTRY (gui->%s), %s);\n",
     {"name", "$title", NULL},
     NULL,
     NULL},


    {"GtkBin", "label",
     NULL,
     {NULL},
     NULL,
     bin_label_handler},

    {"GtkButtonBox", "child_min_width",
     "\tgtk_button_box_set_child_size (GTK_BUTTON_BOX (gui->%s), %s, %s);\n",
     {"name", "child_min_width", "child_min_height", NULL, NULL},
     NULL,
     NULL},

    {"GtkButtonBox", "child_ipad_x",
     "\tgtk_button_box_set_child_ipadding (GTK_BUTTON_BOX (gui->%s), %s, %s);\n",
     {"name", "child_ipad_x", "child_ipad_y", NULL, NULL},
     NULL,
     NULL},

    {"GtkButtonBox", "layout_style",
     "\tgtk_button_box_set_layout (GTK_BUTTON_BOX (gui->%s), %s);\n",
     {"name", "layout_style", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkCalendar", "show_heading",
     NULL,
     {NULL},
     NULL,
     calendar_set_display_options},

    {"GtkCheckButton", "active",
     "\tgtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gui->%s), %s);\n",
     {"name", "active", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkCheckMenuItem", "always_show_toggle",
     "\tgtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (gui->%s), %s);\n",
     {"name", "always_show_toggle", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkCheckMenuItem", "active",
     "\tgtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (gui->%s), %s);\n",
     {"name", "active", NULL, NULL, NULL},
     "active",
     NULL},

    {"GtkCList", "show_titles",
     NULL,
     {NULL},
     NULL,
     clist_show_titles_handler},

    {"GtkCList", "column_widths",
     NULL,
     {NULL},
     NULL,
     clist_set_column_widths_handler},

    {"GtkClock", "seconds",
     "\tgtk_clock_set_seconds (GTK_CLOCK (gui->%s), %s);\n",
     {"name", "seconds", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkClock", "format",
     "\tgtk_clock_set_format (GTK_CLOCK (gui->%s), \"%s\");\n",
     {"name", "format", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkClock", "interval",
     "\tgtk_clock_set_update_interval (GTK_CLOCK (gui->%s), %s);\n",
     {"name", "interval", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkColorSelectionDialog", "policy",
     "\tgtk_color_selection_set_update_policy (GTK_COLOR_SELECTION (GTK_COLOR_SELECTION_DIALOG (gui->%s)->colorsel), %s);\n",
     {"name", "policy", NULL},
     NULL,
     NULL},

    {"GtkCombo", "value_in_list",
     "\tgtk_combo_set_value_in_list (GTK_COMBO (gui->%s), %s, %s);\n",
     {"name", "value_in_list", "ok_if_empty", NULL, NULL},
     NULL,
     NULL},

    {"GtkCombo", "case_sensitive",
     "\tgtk_combo_set_case_sensitive (GTK_COMBO (gui->%s), %s);\n",
     {"name", "case_sensitive", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkCombo", "use_arrows",
     "\tgtk_combo_set_use_arrows (GTK_COMBO (gui->%s), %s);\n",
     {"name", "use_arrows", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkCombo", "use_arrows_always",
     "\tgtk_combo_set_use_arrows_always (GTK_COMBO (gui->%s), %s);\n",
     {"name", "use_arrows_always", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkCombo", "items",
     NULL,
     {NULL},
     NULL,
     combo_items_handler},

    {"GtkContainer", "border_width",
     "\tgtk_container_set_border_width (GTK_CONTAINER (gui->%s), %s);\n",
     {"name", "border_width", NULL},
     NULL,
     NULL},

    {"GtkCurve", "curve_type",
     "\tgtk_curve_set_curve_type (GTK_CURVE (gui->%s), %s);\n",
     {"name", "curve_type", NULL},
     NULL,
     NULL},

    {"GtkCurve", "max_x",
     NULL,
     {NULL},
     NULL,
     curve_set_range_handler},

    {"GtkDial", "view_only",
     "\tgtk_dial_set_view_only (GTK_DIAL (gui->%s), %s);\n",
     {"name", "view_only", NULL},
     NULL,
     NULL},

    {"GtkDial", "update_policy",
     "\tgtk_dial_set_update_policy (GTK_DIAL (gui->%s), %s);\n",
     {"name", "update_policy", NULL},
     NULL,
     NULL},

    {"GtkEntry", "text",
     "\tgtk_entry_set_text (GTK_ENTRY (gui->%s), %s);\n",
     {"name", "$text", NULL},
     NULL,
     NULL},

    {"GtkFileSelection", "show_file_op_buttons",
     NULL,
     {NULL},
     NULL,
     file_selection_fileops_handler},

    {"GtkGammaCurve", "curve_type",
     "\tgtk_curve_set_curve_type (GTK_CURVE (gui->%s->curve), %s);\n",
     {"name", "curve_type", NULL},
     NULL,
     NULL},

    {"GtkGammaCurve", "max_x",
     NULL,
     {NULL},
     NULL,
     curve_set_range_handler},

    {"GtkHRuler", "max_size",
     "\tgtk_ruler_set_range (GTK_RULER (gui->%s), %s, %s, %s, %s);\n",
     {"name", "lower", "upper", "position", "max_size"},
     NULL,
     NULL},

    {"GtkHRuler", "metric",
     "\tgtk_ruler_set_metric (GTK_RULER (gui->%s), %s);\n",
     {"name", "metric", NULL},
     NULL,
     NULL},

    {"GtkLayout", "area_width",
     "\tgtk_layout_set_size (GTK_LAYOUT (gui->%s), %s, %s);\n",
     {"name", "area_width", "area_height", NULL},
     NULL},

    {"GtkLayout", "hstep",
     "\tGTK_ADJUSTMENT (GTK_LAYOUT (gui->%s)->hadjustment)->step_increment = %s;\n",
     {"name", "hstep", NULL},
     NULL},

    {"GtkLayout", "vstep",
     "\tGTK_ADJUSTMENT (GTK_LAYOUT (gui->%s)->vadjustment)->step_increment = %s;\n",
     {"name", "vstep", NULL},
     NULL},

    {"GtkMenuBar", "shadow_type",
     "\tgtk_menu_bar_set_shadow_type (GTK_MENU_BAR (gui->%s), %s);\n",
     {"name", "shadow_type", NULL},
     NULL,
     NULL},

    {"GtkMenuItem", "label",
     "\tgtk_label_parse_uline (GTK_LABEL (GTK_BIN (gui->%s)->child), %s);\n",
     {"name", "$label", NULL},
     NULL,
     NULL},

    {"GtkMenuItem", "right_justify",
     "\tgtk_menu_item_right_justify (GTK_MENU_ITEM (gui->%s));\n",
     {"name", NULL},
     "right_justify",
     NULL},

    {"GtkPixmap", "build_insensitive",
     "\tgtk_pixmap_set_build_insensitive (GTK_PIXMAP (gui->%s), %s);\n",
     {"name", "build_insensitive", NULL},
     NULL,
     NULL},

    {"GtkPaned", "gutter_size",
     "\tgtk_paned_set_gutter_size (GTK_PANED (gui->%s), %s);\n",
     {"name", "gutter_size", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkPaned", "handle_size",
     "\tgtk_paned_set_handle_size (GTK_PANED(gui->%s), %s);\n",
     {"name", "handle_size", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkPaned", "position",
     "\tgtk_paned_set_position (GTK_PANED (gui->%s), %s);\n",
     {"name", "position", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkProgress", "format",
     "\tgtk_progress_set_format_string (GTK_PROGRESS (gui->%s), \"%s\");\n",
     {"name", "format", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkProgress", "value",
     "\tgtk_progress_configure (GTK_PROGRESS (gui->%s), %s, %s, %s);\n",
     {"name", "value", "lower", "upper", NULL},
     NULL,
     NULL},

    {"GtkSpinButton", "snap",
     "\tgtk_spin_button_set_snap_to_ticks (GTK_SPIN_BUTTON (gui->%s), %s);\n",
     {"name", "snap", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkText", "text",
     "\tgtk_text_insert (GTK_TEXT (gui->%s), NULL, NULL, NULL, %s, 14);\n",
     {"name", "$text", NULL},
     NULL,
     NULL},

    {"GtkToolbar", "tooltips",
     "\tgtk_toolbar_set_tooltips (GTK_TOOLBAR (gui->%s), %s);\n",
     {"name", "tooltips", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkTree", "view_mode",
     "\tgtk_tree_set_view_mode (GTK_TREE (gui->%s), %s);\n",
     {"name", "view_mode", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkTree", "selection_mode",
     "\tgtk_tree_set_selection_mode (GTK_TREE (gui->%s), %s);\n",
     {"name", "selection_mode", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkTree", "view_line",
     "\tgtk_tree_set_view_lines (GTK_TREE (gui->%s), %s);\n",
     {"name", "view_line", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkVRuler", "max_size",
     "\tgtk_ruler_set_range (GTK_RULER (gui->%s), %s, %s, %s, %s);\n",
     {"name", "lower", "upper", "position", "max_size"},
     NULL,
     NULL},

    {"GtkVRuler", "metric",
     "\tgtk_ruler_set_metric (GTK_RULER (gui->%s), %s);\n",
     {"name", "metric", NULL, NULL, NULL},
     NULL,
     NULL},

    {"GtkWidget", "tooltip",
     "\tgtk_tooltips_set_tip (gui->tooltips, GTK_WIDGET (gui->%s), _(%s), NULL);\n",
     {"name", "$tooltip", NULL, NULL, NULL},
     NULL,
     NULL},

    {NULL, NULL,
     NULL,
     {NULL},
     NULL,
     NULL}
};

/* Signal list management
 * 
 * An interesting problem arises when you map more than one signal handler 
 * to the same handler.  e.g. both a menu item and a button use the
 * function on_file_open() to handle the "activated" and "clicked" events.
 * 
 * We can add the signal handler twice, like so:
 *    void on_file_open (GtkPixmapMenuItem *widget, gpointer user_data);
 *    void on_file_open (GtkButton *widget, gpointer user_data);
 * 
 * which is what we use to do.  But that doesn't work, since C doesn't like
 * function overloading.  So we can do this:
 *    void on_file_open (GtkWidget *widget, gpointer user_data);
 * 
 * and have this function map to both signals.  As long as the signal handler
 * prototypes are the same, this works.  If they aren't, then the programmer
 * needs to find a different naming scheme.
 * 
 * To help out, the functions below manage a list of signal handlers that
 * have already been added, so that we don't add them twice.  These functions
 * will be called from g2c_doc.c and g2c_widget.c.
 */
void
add_to_signal_list(gchar * signal_name)
{
    signal_list = g_list_append(signal_list, signal_name);
}

void
clear_signal_list(void)
{
    g_list_free(signal_list);
    signal_list = NULL;
}

gboolean
is_in_signal_list(gchar * signal_name)
{
    GList *temp_list = NULL;
    temp_list =
	g_list_find_custom(signal_list, signal_name,
			   g2c_string_list_compare_cb);

    if (temp_list == NULL)
	return 0;
    else
	return 1;
}


/* Special Handlers */
void
bin_label_handler(g2cWidget * widget)
{
    gchar *action = NULL;
    gchar *accel_group = NULL;
    gchar *mask = NULL;

    g_assert(widget != NULL);

    if (g_type_is_a(widget->klass, GTK_TYPE_BUTTON)) {
	action = g_strdup("clicked");
    } else if (g_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM)) {
	action = g_strdup("activate_item");
    }

    if (widget->parent != NULL &&
	g_type_is_a(widget->parent->klass, GTK_TYPE_MENU)) {
	accel_group = g_strdup_printf("%s_accel_group", widget->parent->name);
	mask = g_strdup("0");
    } else {
	accel_group = g_strdup("accel_group");
	mask = g_strdup("GDK_MOD1_MASK");
    }

    /* Option menus don't have accelerators! */
    if (widget->parent != NULL && widget->parent->parent != NULL &&
	!g_type_is_a(widget->parent->parent->klass, GTK_TYPE_OPTION_MENU)) {
	if (CURRENT_PROJECT->gnome_support
	    && (g_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM)
		|| (g_type_is_a(widget->parent->klass, GTK_TYPE_TOOLBAR)
		    && strcmp(widget->klass_name, "GtkButton") != 0))) {
	    /* Ignore accels */
	} else if (g2c_widget_get_property(widget, "stock_pixmap") == NULL) {
	    gchar *str = g2c_widget_get_property(widget, "label");

	    str = g2c_stringify(str);

	    /* Also, don't do this for Gnome menus */
	    fprintf(CURRENT_FILE,
		    "\t%s_key = gtk_label_parse_uline (GTK_LABEL (GTK_BIN (gui->%s)->child),\n"
		    "\t                                %s);\n",
		    widget->name, widget->name, str);

	    fprintf(CURRENT_FILE,
		    "\tgtk_widget_add_accelerator (GTK_WIDGET (gui->%s), \"%s\", \n"
		    "\t                             gui->%s, %s_key, %s, 0);\n",
		    widget->name, action, accel_group, widget->name, mask);

	    g_free(str);
	}
    }

    g_free(action);
    g_free(accel_group);
    g_free(mask);
}

void
calendar_set_display_options(g2cWidget * widget)
{
    gchar *option_names[5] = { "GTK_CALENDAR_SHOW_HEADING",
	"GTK_CALENDAR_SHOW_DAY_NAMES",
	"GTK_CALENDAR_NO_MONTH_CHANGE",
	"GTK_CALENDAR_SHOW_WEEK_NUMBERS",
	"GTK_CALENDAR_WEEK_START_MONDAY"
    };
    gchar **options = NULL;
    gchar *option_string = NULL;
    guint i = 0;

    g_assert(widget != NULL);

    options = g_new0(gchar *, 6);

    if (g2c_get_bool(g2c_widget_get_property(widget, "show_headings")))
	options[i++] = g_strdup(option_names[0]);

    if (g2c_get_bool(g2c_widget_get_property(widget, "show_day_names")))
	options[i++] = g_strdup(option_names[1]);

    if (g2c_get_bool(g2c_widget_get_property(widget, "no_month_change")))
	options[i++] = g_strdup(option_names[2]);

    if (g2c_get_bool(g2c_widget_get_property(widget, "show_week_numbers")))
	options[i++] = g_strdup(option_names[3]);

    if (g2c_get_bool(g2c_widget_get_property(widget, "week_start_monday")))
	options[i++] = g_strdup(option_names[4]);

    if (i > 0)
	option_string =  g_strjoinv(" |\n\t                              ",
				    (gchar **) options);
    else
	option_string = g_strdup("");

    fprintf(CURRENT_FILE,
	    "\tgtk_calendar_display_options (GTK_CALENDAR (gui->%s),\n"
	    "\t                              %s);\n",
	    widget->name, option_string);

    g_free(option_string);
    g_strfreev(options);
}

void
clist_show_titles_handler(g2cWidget * widget)
{
    g_assert(widget != NULL);

    if (g2c_get_bool(g2c_widget_get_property(widget, "show_titles")))
	fprintf(CURRENT_FILE,
		"\tgtk_clist_column_titles_show (GTK_CLIST (gui->%s));\n",
		widget->name);
    else
	fprintf(CURRENT_FILE,
		"\tgtk_clist_column_titles_hide (GTK_CLIST (gui->%s));\n",
		widget->name);
}

void
clist_set_column_widths_handler(g2cWidget * widget)
{
    gchar **cols = NULL;
    gint i = 0;

    g_assert(widget != NULL);

    cols = g_strsplit(g2c_widget_get_property(widget, "column_widths"),
		      ",", 0);

    while (cols[i] != NULL) {
	fprintf(CURRENT_FILE,
		"\tgtk_clist_set_column_width (GTK_CLIST (gui->%s), %d, %s);\n",
		widget->name, i, cols[i]);

	i++;
    }

    g_strfreev(cols);
}

void
combo_items_handler(g2cWidget * widget)
{
    gchar **items = NULL;
    gint i = 0;
    gchar *str = NULL;

    g_assert(widget != NULL);

    items = g_strsplit(g2c_widget_get_property(widget, "items"), "\n", 0);

    while (items[i] != NULL) {
	str = g2c_stringify(items[i]);

	fprintf(CURRENT_FILE,
		"\t%s_items = g_list_append (%s_items, %s);\n",
		widget->name, widget->name, str);

	g_free(str);
	str = NULL;

	i++;
    }

    fprintf(CURRENT_FILE,
	    "\tgtk_combo_set_popdown_strings (GTK_COMBO (gui->%s), %s_items);\n",
	    widget->name, widget->name);

    g_strfreev(items);
}

void
curve_set_range_handler(g2cWidget * widget)
{
    g_assert(widget != NULL);

    if (strcmp(widget->klass_name, "GtkGammaCurve") == 0) {
	fprintf(CURRENT_FILE,
		"\tgtk_curve_set_range (GTK_CURVE (gui->%s->curve), %s, %s, %s, %s);\n",
		widget->name,
		g2c_widget_get_property(widget, "min_x"),
		g2c_widget_get_property(widget, "max_x"),
		g2c_widget_get_property(widget, "min_y"),
		g2c_widget_get_property(widget, "max_y"));
    } else {
	fprintf(CURRENT_FILE,
		"\tgtk_curve_set_range (GTK_CURVE (gui->%s), %s, %s, %s, %s);\n",
		widget->name,
		g2c_widget_get_property(widget, "min_x"),
		g2c_widget_get_property(widget, "max_x"),
		g2c_widget_get_property(widget, "min_y"),
		g2c_widget_get_property(widget, "max_y"));
    }
}

void
file_selection_fileops_handler(g2cWidget * widget)
{
    g_assert(widget != NULL);

    if (g2c_get_bool(g2c_widget_get_property(widget, "show_file_op_buttons"))) {
	fprintf(CURRENT_FILE,
		"\tgtk_file_selection_show_fileop_buttons (GTK_FILE_SELECTION (gui->%s));\n",
		widget->name);
    } else {
	fprintf(CURRENT_FILE,
		"\tgtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (gui->%s));\n",
		widget->name);
    }
}

void
option_menu_initial_choice_handler(g2cWidget * widget)
{
    g_assert(widget != NULL);

    fprintf(CURRENT_FILE,
	    "\tgtk_option_menu_set_history (GTK_OPTION_MENU (gui->%s), %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget, "initial_choice"));
}

/* Creation handlers */

void
create_custom_widget(g2cWidget * widget)
{
    gchar **params = NULL;
    gchar *param_string = NULL;
    gint i = 0;

    g_assert(widget != NULL);

    params = g_new0(gchar *, 5);

    /* There are several attributes that determine the behaviour of the dock item */
    if (g2c_widget_get_property(widget, "string1") != NULL)
	params[i++] =
	    g2c_stringify(g2c_widget_get_property(widget, "string1"));

    if (g2c_widget_get_property(widget, "string2") != NULL)
	params[i++] =
	    g2c_stringify(g2c_widget_get_property(widget, "string2"));

    if (g2c_widget_get_property(widget, "int1") != NULL)
	params[i++] =
	    g_strdup_printf("%s", g2c_widget_get_property(widget, "int1"));

    if (g2c_widget_get_property(widget, "int2") != NULL)
	params[i++] =
	    g_strdup_printf("%s", g2c_widget_get_property(widget, "int2"));

    if (i > 0)
	param_string = g_strjoinv(", ", params);
    else
	param_string = g_strdup("");

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GtkWidget*) %s (%s);\n",
	    widget->name,
	    g2c_widget_get_property(widget, "creation_function"),
	    param_string);

    g_strfreev(params);
    g_free(param_string);
}

void
create_gnome_app(g2cWidget * widget)
{
    gchar *title = NULL;

    g_assert(widget != NULL);

    if (g2c_widget_get_property(widget, "title") != NULL)
	title = g2c_stringify(g2c_widget_get_property(widget, "title"));
    else
	title = g_strdup("");

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeApp*) gnome_app_new (\"%s\", %s);\n",
	    widget->name, CURRENT_PROJECT->program_name, title);

    g_free(title);
}

void
create_gnome_date_edit(g2cWidget * widget)
{
    /* Create the widget, then set some properties */
    gchar *week_start_monday = "GNOME_DATE_EDIT_WEEK_STARTS_ON_MONDAY";
    gchar *show_time = "GNOME_DATE_EDIT_SHOW_TIME";
    gchar *use_24_format = "GNOME_DATE_EDIT_24_HR";
    gchar *flags = NULL;

    g_assert(widget != NULL);

    if (g2c_widget_get_property(widget, "show_time") == NULL)
	flags = g_strdup(show_time);

    if (g2c_widget_get_property(widget, "week_start_monday") == NULL) {
	if (flags != NULL)
	    g_strjoin(" | ", flags, week_start_monday);
	else
	    flags = g_strdup(week_start_monday);
    }

    if (g2c_widget_get_property(widget, "use_24_format") == NULL) {
	if (flags != NULL)
	    g_strjoin(" |\n", flags, use_24_format);
	else
	    flags = g_strdup(use_24_format);
    }

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeDateEdit*) gnome_date_edit_new((time_t) 0, %s, %s);\n",
	    widget->name,
	    g2c_get_bool_s(g2c_widget_get_property(widget, "show_time")),
	    g2c_get_bool_s(g2c_widget_get_property
			   (widget, "use_24_format")));

    if (flags != NULL) {
	fprintf(CURRENT_FILE,
		"\tgnome_date_edit_set_flags(gui->%s, %s);\n",
		widget->name, flags);
    }
}

void
create_gnome_dock(g2cWidget * widget)
{
    g_assert(widget != NULL);

    fprintf(CURRENT_FILE,
	    "\tgui->%s = GNOME_DOCK (GNOME_APP (gui->%s)->dock);\n",
	    widget->name, g2c_widget_get_top_parent(widget)->name);
}

void
create_gnome_dock_item(g2cWidget * widget)
{
    gchar **behaviors = NULL;
    gchar *behavior = NULL;
    gint i = 0;

    g_assert(widget != NULL);

    behaviors = g_new0(gchar *, 6);

    /* There are several attributes that determine the behaviour of the dock item */
    if (g2c_get_bool(g2c_widget_get_property(widget, "exclusive")))
	behaviors[i++] = g_strdup("GNOME_DOCK_ITEM_BEH_EXCLUSIVE");

    if (g2c_get_bool(g2c_widget_get_property(widget, "never_horizontal")))
	behaviors[i++] = g_strdup("GNOME_DOCK_ITEM_BEH_NEVER_HORIZONTAL");

    if (g2c_get_bool(g2c_widget_get_property(widget, "never_vertical")))
	behaviors[i++] = g_strdup("GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL");

    if (g2c_get_bool(g2c_widget_get_property(widget, "never_floating")))
	behaviors[i++] = g_strdup("GNOME_DOCK_ITEM_BEH_NEVER_FLOATING");

    if (g2c_get_bool(g2c_widget_get_property(widget, "locked")))
	behaviors[i++] = g_strdup("GNOME_DOCK_ITEM_BEH_LOCKED");

    if (i > 0)
	behavior = g_strjoinv(" |\n\t           ", behaviors);
    else
	behavior = g_strdup("GNOME_DOCK_ITEM_BEH_NORMAL");

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeDockItem*) gnome_dock_item_new (\"%s\",\n"
	    "\t           %s);\n", widget->name, widget->name, behavior);

    g_free(behavior);
    g_strfreev(behaviors);
}

void
gnome_druid_page_start_logo_image_handler(g2cWidget * widget)
{
    fprintf(CURRENT_FILE,
	    "\tgnome_druid_page_start_set_logo (GNOME_DRUID_PAGE_START (gui->%s),\n"
	    "\t                                 create_image (\"%s/%s\"));\n",
	    widget->name,
	    CURRENT_PROJECT->name,
	    g2c_widget_get_property(widget, "logo_image"));
}

void
gnome_druid_page_start_watermark_image_handler(g2cWidget * widget)
{
    fprintf(CURRENT_FILE,
	    "\tgnome_druid_page_start_set_watermark (GNOME_DRUID_PAGE_START (gui->%s),\n"
	    "\t                                      create_image (\"%s/%s\"));\n",
	    widget->name,
	    CURRENT_PROJECT->name,
	    g2c_widget_get_property(widget, "watermark_image"));
}

void
gnome_druid_page_standard_logo_image_handler(g2cWidget * widget)
{
    fprintf(CURRENT_FILE,
	    "\tgnome_druid_page_standard_set_logo (GNOME_DRUID_PAGE_STANDARD (gui->%s),\n"
	    "\t                                         create_image (\"%s/%s\"));\n",
	    widget->name,
	    CURRENT_PROJECT->name,
	    g2c_widget_get_property(widget, "logo_image"));
}

void
gnome_druid_page_finish_logo_image_handler(g2cWidget * widget)
{
    fprintf(CURRENT_FILE,
	    "\tgnome_druid_page_finish_set_logo (GNOME_DRUID_PAGE_FINISH (gui->%s),\n"
	    "\t                                  create_image (\"%s/%s\"));\n",
	    widget->name,
	    CURRENT_PROJECT->name,
	    g2c_widget_get_property(widget, "logo_image"));
}

void
gnome_druid_page_finish_watermark_image_handler(g2cWidget * widget)
{
    fprintf(CURRENT_FILE,
	    "\tgnome_druid_page_finish_set_watermark (GNOME_DRUID_PAGE_FINISH (gui->%s),\n"
	    "\t                                       create_image (\"%s/%s\"));\n",
	    widget->name,
	    CURRENT_PROJECT->name,
	    g2c_widget_get_property(widget, "watermark_image"));
}

void
create_gnome_entry(g2cWidget * widget)
{
    gchar *history_id = NULL;

    history_id =
	g_strdup_printf("\"%s\"",
			g2c_widget_get_property(widget, "history_id"));

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeEntry*) gnome_entry_new (%s);\n",
	    widget->name,
	    g2c_widget_get_property(widget,
				    "history_id") ? history_id : "NULL");

    g_free(history_id);
}

void
create_gnome_file_entry(g2cWidget * widget)
{
    gchar *history_id = NULL;
    gchar *title = NULL;

    history_id =
	g_strdup_printf("\"%s\"",
			g2c_widget_get_property(widget, "history_id"));
    title = g2c_stringify(g2c_widget_get_property(widget, "title"));

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeFileEntry*) gnome_file_entry_new (%s, %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget,
				    "history_id") ? history_id : "NULL",
	    g2c_widget_get_property(widget, "title") ? title : "NULL");

    g_free(history_id);
    g_free(title);
}

void
create_gnome_icon_entry(g2cWidget * widget)
{
    gchar *history_id = NULL;
    gchar *title = NULL;

    history_id =
	g_strdup_printf("\"%s\"",
			g2c_widget_get_property(widget, "history_id"));
    title = g2c_stringify(g2c_widget_get_property(widget, "title"));

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeIconEntry*) gnome_icon_entry_new (%s, %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget,
				    "history_id") ? history_id : "NULL",
	    g2c_widget_get_property(widget, "title") ? title : "NULL");

    g_free(history_id);
    g_free(title);
}

void
create_gnome_icon_list(g2cWidget * widget)
{
    GString *flags = NULL;

    flags = g_string_new("");

    if (g2c_get_bool(g2c_widget_get_property(widget, "text_editable"))) {
	g_string_append(flags, "GNOME_ICON_LIST_IS_EDITABLE");
    }

    if (g2c_get_bool(g2c_widget_get_property(widget, "text_static"))) {
	if (flags->len)
	    g_string_append(flags, " | ");
	g_string_append(flags, "GNOME_ICON_LIST_STATIC_TEXT");
    }

    if (flags->len == 0)
	g_string_append(flags, "0");

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeIconList*) gnome_icon_list_new_flags (%s, NULL, %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget, "icon_width"), flags->str);

    g_string_free(flags, TRUE);
}

void
create_gnome_number_entry(g2cWidget * widget)
{
    gchar *history_id = NULL;
    gchar *title = NULL;

    history_id =
	g_strdup_printf("\"%s\"",
			g2c_widget_get_property(widget, "history_id"));
    title = g2c_stringify(g2c_widget_get_property(widget, "title"));

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomeNumberEntry*) gnome_number_entry_new (%s, %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget,
				    "history_id") ? history_id : "NULL",
	    g2c_widget_get_property(widget, "title") ? title : "NULL");

    g_free(history_id);
    g_free(title);
}

void
create_gnome_pixmap_entry(g2cWidget * widget)
{
    gchar *history_id = NULL;
    gchar *title = NULL;
    gchar *preview = NULL;

    history_id =
	g_strdup_printf("\"%s\"",
			g2c_widget_get_property(widget, "history_id"));
    title = g2c_stringify(g2c_widget_get_property(widget, "title"));
    preview = g2c_get_bool_s(g2c_widget_get_property(widget, "preview"));

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GnomePixmapEntry*) gnome_pixmap_entry_new (%s, %s, %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget,
				    "history_id") ? history_id : "NULL",
	    g2c_widget_get_property(widget, "title") ? title : "NULL",
	    preview);

    g_free(history_id);
    g_free(title);
    g_free(preview);
}

void
create_gtk_aspect_frame(g2cWidget * widget)
{
    gchar *label = NULL;

    g_assert(widget != NULL);

    label = g2c_widget_get_property(widget, "label");
    if (label == NULL)
	label = g_strdup("NULL");
    else
	label = g2c_stringify(label);

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GtkAspectFrame*) gtk_aspect_frame_new (%s, %s, %s, %s, %s);\n",
	    widget->name,
	    label,
	    g2c_widget_get_property(widget, "xalign"),
	    g2c_widget_get_property(widget, "yalign"),
	    g2c_widget_get_property(widget, "ratio"),
	    g2c_get_bool_s(g2c_widget_get_property(widget, "obey_child")));

    g_free(label);
}

void
create_gtk_button(g2cWidget * widget)
{
    g_assert(widget != NULL);

    if (widget->parent != NULL && widget->internal_child != NULL) {
	g2cWidget *parent = g2c_widget_find_non_internal_parent(widget);

	if (g_type_is_a(parent->klass, GTK_TYPE_FILE_SELECTION)) {
	    if (!strcmp(widget->internal_child, "cancel_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_FILE_SELECTION (gui->%s)->cancel_button;\n",
			widget->name, parent->name);
	    } else if (!strcmp(widget->internal_child, "ok_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_FILE_SELECTION (gui->%s)->ok_button;\n",
			widget->name, widget->parent->name);
	    } else if (!strcmp(widget->internal_child, "help_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_FILE_SELECTION (gui->%s)->help_button;\n",
			widget->name, widget->parent->name);
	    }
	} else if (g_type_is_a(parent->klass, GTK_TYPE_COLOR_SELECTION_DIALOG)) {
	    if (!strcmp(widget->internal_child, "ok_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_COLOR_SELECTION_DIALOG (gui->%s)->ok_button;\n",
		    widget->name, widget->parent->name);
	    } else if (!strcmp(widget->internal_child, "cancel_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_COLOR_SELECTION_DIALOG (gui->%s)->cancel_button;\n",
			widget->name, widget->parent->name);
	    } else if (!strcmp(widget->internal_child, "help_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_COLOR_SELECTION_DIALOG (gui->%s)->help_button;\n",
			widget->name, widget->parent->name);
	    }
	} else if (g_type_is_a(parent->klass, GTK_TYPE_FONT_SELECTION_DIALOG)){
	    if (!strcmp(widget->internal_child, "ok_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_FONT_SELECTION_DIALOG (gui->%s)->ok_button;\n",
			widget->name, widget->parent->name);
	    } else if (!strcmp(widget->internal_child, "cancel_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_FONT_SELECTION_DIALOG (gui->%s)->cancel_button;\n",
			widget->name, widget->parent->name);
	    } else if (!strcmp(widget->internal_child, "apply_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) GTK_FONT_SELECTION_DIALOG (gui->%s)->apply_button;\n",
			widget->name, widget->parent->name);
	    }
	} else if (g_type_is_a(parent->klass, GTK_TYPE_INPUT_DIALOG)) {
	    if (!strcmp(widget->internal_child, "save_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) gui->%s->save_button;\n",
			widget->name, widget->parent->name);
	    } else if (!strcmp(widget->internal_child, "close_button")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = (GtkButton*) gui->%s->close_button;\n",
			widget->name, widget->parent->name);
	    }
	} else {
	    fprintf(CURRENT_FILE,
		    "\tgui->%s = (GtkButton*) gtk_button_new_with_label (\"\");\n",
		    widget->name);
	}
    } else {
	fprintf(CURRENT_FILE,
		"\tgui->%s = (GtkButton*) gtk_button_new_with_label (\"\");\n",
		widget->name);
    }
}

void
create_gtk_pixmap(g2cWidget *widget)
{
    /* Need to use the file name to create a new GdkPixmap and GdkMask,
     * then pass that in to the create call
     */

    if (CURRENT_PROJECT->gnome_support) {
	fprintf(CURRENT_FILE,
		"\tgui->%s = (GtkPixmap*) create_pixmap (GTK_WIDGET (gui->%s), \"%s\", FALSE);\n",
		widget->name,
		widget->parent->name,
		g2c_widget_get_property(widget, "filename"));
    } else {
	fprintf(CURRENT_FILE,
		"\tgui->%s = (GtkPixmap*) create_pixmap (GTK_WIDGET (gui->%s), \"%s\");\n",
		widget->name,
		widget->parent->name,
		g2c_widget_get_property(widget, "filename"));
    }
}

void
create_gtk_entry(g2cWidget *widget)
{
    g_assert(widget != NULL);

    if (widget->parent != NULL) {
	if (strcmp(widget->parent->klass_name, "GtkCombo") == 0) {
	    fprintf(CURRENT_FILE,
		    "\tgui->%s = GTK_ENTRY (GTK_COMBO (gui->%s)->entry);\n",
		    widget->name, widget->parent->name);
	}
#ifdef USE_GNOME
	else if (g_type_is_a(widget->parent->klass, GNOME_TYPE_ENTRY)) {
	    fprintf(CURRENT_FILE,
		    "\tgui->%s = GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY(gui->%s)));\n",
		    widget->name, widget->parent->name);
	} else if (g_type_is_a(widget->parent->klass, GNOME_TYPE_FILE_ENTRY)) {
	    fprintf(CURRENT_FILE,
		    "\tgui->%s = GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY(gui->%s)));\n",
		    widget->name, widget->parent->name);
	} else {
	    fprintf(CURRENT_FILE,
		    "\tgui->%s = (GtkEntry*) gtk_entry_new ();\n",
		    widget->name);
	}
#endif
    } else {
	fprintf(CURRENT_FILE,
		"\tgui->%s = (GtkEntry*) gtk_entry_new ();\n",
		widget->name);
    }
}

void
create_gtk_hscale(g2cWidget * widget)
{
    g_assert(widget != NULL);

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GtkHScale*) gtk_hscale_new (GTK_ADJUSTMENT (gtk_adjustment_new (%s,%s,%s,%s,%s,%s)));\n",
	    widget->name,
	    g2c_widget_get_property(widget, "value"),
	    g2c_widget_get_property(widget, "lower"),
	    g2c_widget_get_property(widget, "upper"),
	    g2c_widget_get_property(widget, "step"),
	    g2c_widget_get_property(widget, "page"),
	    g2c_widget_get_property(widget, "page_size"));
}

void
create_gtk_spin_button(g2cWidget * widget)
{
    g_assert(widget != NULL);

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GtkSpinButton*) gtk_spin_button_new (\n"
	    "\t          GTK_ADJUSTMENT (gtk_adjustment_new (%s,%s,%s,%s,%s,%s)), %s, %s);\n",
	    widget->name,
	    g2c_widget_get_property(widget, "value"),
	    g2c_widget_get_property(widget, "lower"),
	    g2c_widget_get_property(widget, "upper"),
	    g2c_widget_get_property(widget, "step"),
	    g2c_widget_get_property(widget, "page"),
	    g2c_widget_get_property(widget, "page_size"),
	    g2c_widget_get_property(widget, "climb_rate"),
	    g2c_widget_get_property(widget, "digits"));
}

void
create_gtk_vbox(g2cWidget * widget)
{
    gchar *homogeneous = NULL;
    gchar *spacing = NULL;

    g_assert(widget != NULL);

    if (widget->internal_child == NULL) {
	homogeneous =
	    g2c_get_bool_s(g2c_widget_get_property(widget, "homogeneous"));
	spacing =
	    g2c_get_bool_s(g2c_widget_get_property(widget, "spacing"));

	fprintf(CURRENT_FILE,
		"\tgui->%s = (GtkVBox*) gtk_vbox_new (%s,%s);\n",
		widget->name, homogeneous, spacing);
    } else {
	g2cWidget *parent = g2c_widget_find_non_internal_parent(widget);

	if (g_type_is_a(parent->klass, GTK_TYPE_DIALOG)) {
	    if (!strcmp(widget->internal_child, "vbox")) {
		fprintf(CURRENT_FILE,
			"\tgui->%s = GTK_VBOX(gui->%s->vbox);\n",
			widget->name, widget->parent->name);
	    }
	}
#ifdef USE_GNOME
	else if (g_type_is_a(parent->klass, GNOME_TYPE_DRUID_PAGE_STANDARD)) {
	    if (!strcmp(widget->internal_child, "vbox")) {
		fprintf(CURRENT_FILE, "\tgui->%s = GTK_VBOX(gui->%s->vbox);\n",
			widget->name, widget->parent->name);
	    }
	}
    }
#endif

    g_free(homogeneous);
    g_free(spacing);
}

void
create_gtk_vscale(g2cWidget * widget)
{
    g_assert(widget != NULL);

    fprintf(CURRENT_FILE,
	    "\tgui->%s = (GtkVScale*) gtk_vscale_new (GTK_ADJUSTMENT (gtk_adjustment_new (%s,%s,%s,%s,%s,%s)));\n",
	    widget->name,
	    g2c_widget_get_property(widget, "value"),
	    g2c_widget_get_property(widget, "lower"),
	    g2c_widget_get_property(widget, "upper"),
	    g2c_widget_get_property(widget, "step"),
	    g2c_widget_get_property(widget, "page"),
	    g2c_widget_get_property(widget, "page_size"));
}

/* Helpers */

static gint
compare_strings(gconstpointer data, gconstpointer user_data)
{
    if (data == NULL || user_data == NULL)
	return 1;
    else
	return strcmp((gchar *) data, (gchar *) user_data);
}

static void
free_children(gpointer data, gpointer user_data)
{
    g2c_widget_destroy((g2cWidget *) data);
}

static void
free_signals(gpointer data, gpointer user_data)
{
    g2c_signal_destroy((g2cSignal *) data);
}

static void
free_accelerators(gpointer data, gpointer user_data)
{
    g2c_accel_destroy((g2cAccel *) data);
}

static void
free_radio_groups(gpointer data, gpointer user_data)
{
    g_free((gchar *) data);
}

/* Class Functions */

g2cWidget *
g2c_widget_new(const gchar * class_name)
{
    /* Create a new widget struct */
    g2cWidget *widget = g_new0(g2cWidget, 1);

    allocs = allocs + 1;

    /* Create a new hash table for properties */
    widget->properties = g_hash_table_new(g_str_hash, g_str_equal);

    widget->children = NULL;
    widget->signals = NULL;
    widget->parent = NULL;
    widget->accelerators = NULL;
    widget->radio_groups = NULL;
    widget->internal_child = NULL;
    widget->is_bonobo_control = FALSE;

    widget->klass_name = g_strdup(class_name);
    widget->name = NULL;

    widget->klass = g_type_from_name(widget->klass_name);

    if (widget->klass != G_TYPE_INVALID) {
	if (gtk_type_class(widget->klass) == NULL)
	    g_print("Invalid type: %s\n", widget->klass_name);
    } else {
	if (!strcmp(widget->klass_name, "GtkMenuBar"))
	    widget->klass = GTK_TYPE_MENU_SHELL;
	else if (!strcmp(widget->klass_name, "GtkCombo"))
	    widget->klass = GTK_TYPE_HBOX;
	else if (!strcmp(widget->klass_name, "GtkGammaCurve"))
	    widget->klass = GTK_TYPE_VBOX;
	else if (!strcmp(widget->klass_name, "GtkPixmapMenuItem"))
	    widget->klass = GTK_TYPE_MENU_ITEM;
	else if (!strcmp(widget->klass_name, "GtkHPaned"))
	    widget->klass = GTK_TYPE_PANED;
	else if (!strcmp(widget->klass_name, "GtkVPaned"))
	    widget->klass = GTK_TYPE_PANED;
	else if (!strcmp(widget->klass_name, "GtkHButtonBox"))
	    widget->klass = GTK_TYPE_BUTTON_BOX;
	else if (!strcmp(widget->klass_name, "GtkVButtonBox"))
	    widget->klass = GTK_TYPE_BUTTON_BOX;
	else if (!strcmp(widget->klass_name, "GtkVRuler"))
	    widget->klass = GTK_TYPE_RULER;
	else if (!strcmp(widget->klass_name, "GtkHRuler"))
	    widget->klass = GTK_TYPE_RULER;
	else if (!strcmp(widget->klass_name, "GtkStatusbar"))
	    widget->klass = GTK_TYPE_HBOX;
	else if (!strcmp(widget->klass_name, "Custom"))
	    widget->klass = GTK_TYPE_WIDGET;
#ifdef ENABLE_BONOBO
	/* Check for a bonobo class */
	else if ((strncmp(widget->klass_name, "OAFIID:", 7) == 0) ||
		 (strncmp(widget->klass_name, "oafiid:", 7) == 0)) {
	    /* This is a bonobo widget */
	    CURRENT_PROJECT->has_bonobo_controls = TRUE;

	    /* This is a bonobo widget */
	    g_free(widget->klass_name);
	    widget->klass_name = g_strdup("GtkWidget");
	    g2c_widget_set_property(widget, "OAFIID",
				    class_name + sizeof(gchar) * 7);

	    printf("Found a bonobo control: %s\n",
		   class_name + sizeof(gchar) * 7);
	    widget->is_bonobo_control = TRUE;
	    widget->klass = GTK_TYPE_WIDGET;
	}
#endif

	else {
	    g_print("Unhandled class, %s, set to GtkWidget\n",
		    widget->klass_name);
	    widget->klass = GTK_TYPE_WIDGET;
	}
    }

    return widget;
}

void
g2c_widget_destroy(g2cWidget * widget)
{
    allocs = allocs - 1;

    /* Deallocate the properties hash table */
    g_hash_table_foreach(widget->properties,
			 g2c_hash_element_free_cb, NULL);

    g_hash_table_destroy(widget->properties);

    /* Free the sub-widgets */
    if (widget->children) {
	g_list_foreach(widget->children, free_children, NULL);

	g_list_free(widget->children);
    }

    if (widget->signals) {
	g_list_foreach(widget->signals, free_signals, NULL);

	g_list_free(widget->signals);
    }

    if (widget->accelerators) {
	g_list_foreach(widget->accelerators, free_accelerators, NULL);

	g_list_free(widget->accelerators);
    }

    if (widget->radio_groups) {
	g_list_foreach(widget->radio_groups, free_radio_groups, NULL);

	g_list_free(widget->radio_groups);
    }

    g_free(widget->name);
    g_free(widget->internal_child);
    g_free(widget->klass_name);

    g_free(widget);
}

g2cWidget *
g2c_widget_get_top_parent(g2cWidget * widget)
{
    g2cWidget *result = NULL;
    result = widget;

    while (result->parent != NULL) {
	result = result->parent;
    }

    return result;
}

void
g2c_widget_set_order(g2cWidget * widget, gint order)
{
    widget->order = order;
}

void
g2c_widget_set_property(g2cWidget * widget,
			const gchar * name, const gchar * value)
{
    if (strcmp(name, "name") == 0) {
	g_free(widget->name);

	/* We don't want any "-" in the name, so convert to "_" */
	widget->name = g_strdup(value);
	widget->name = g_strdelimit(widget->name, ":-", '_');
    } else if (strcmp(name, "class") == 0) {
	g_free(widget->klass_name);
	widget->klass_name = g_strdup(value);
    } else if (strcmp(name, "internal_child") == 0) {
	g_free(widget->internal_child);
	widget->internal_child = g_strdup(value);
    } else {
	/* Add to the list of managed properties */
	g_hash_table_insert(widget->properties,
			    g_strdup(name), g_strdup(value));
    }
}

gchar *
g2c_widget_get_property(g2cWidget * widget, const gchar * name)
{
    if (name == NULL)
	return NULL;
    if (strcmp(name, "name") == 0)
	return widget->name;
    else if (strcmp(name, "class") == 0)
	return widget->klass_name;
    else
	return g_hash_table_lookup(widget->properties, name);
}

void
g2c_widget_add_subwidget(g2cWidget * widget, g2cWidget * subwidget)
{
    subwidget->parent = widget;

    if (g_type_is_a(widget->klass, GTK_TYPE_NOTEBOOK) &&
	g_type_is_a(subwidget->klass, GTK_TYPE_LABEL)) {
	/* Special treatment for notebook tab labels.
	 * The last widget appended will be the widget
	 * that is added to the notebook.  This label widget
	 * is the tab's label for the aforementioned widget
	 */
	g2c_widget_set_property((g2cWidget *)
				g_list_last(widget->children)->data,
				"_tab_label", subwidget->name);

	widget->children = g_list_prepend(widget->children, subwidget);
    } else
	widget->children = g_list_append(widget->children, subwidget);
}

void
g2c_widget_add_signal(g2cWidget * widget, g2cSignal * signal)
{
    widget->signals = g_list_append(widget->signals, signal);
}

void
g2c_widget_add_accel(g2cWidget * widget, g2cAccel * accel)
{
    widget->accelerators = g_list_append(widget->accelerators, accel);
}

void
g2c_widget_add_focus_target(g2cWidget * widget, const gchar * focus_target)
{
    g2cAccel *accel = NULL;
    g2cWidget *parent = NULL;
    gchar *key = NULL;

    g_assert(widget != NULL);
    g_assert(focus_target != NULL);

    key = g_strconcat(focus_target, "_", widget->name, "_key", NULL);

    accel = g2c_accel_new();
    g2c_accel_set_modifiers(accel, "GDK_MOD1_MASK");
    g2c_accel_set_key(accel, key);

    /* We don't set the signal, since it depends on the type of the focus target widget */
    g2c_accel_set_focus_target(accel, focus_target);

    /* We add this accelerator to the top-level widget.  We do this to simplify processing
     * later on.  If a focus_target is present in an accelerator, then we do special processing.  
     * See g2c_doc.c::output_widget_files.
     */
    parent = g2c_widget_get_top_parent(widget);

    g2c_widget_add_accel(parent, accel);

    g_free(key);
}

void
g2c_widget_add_radio_group(g2cWidget * widget, const gchar * group_name)
{
    /* Does this group already exist? */
    GList *list = NULL;
    gchar *temp = g_strdup(group_name);
    g2cWidget *top_parent = g2c_widget_get_top_parent(widget);

    list = g_list_find_custom(top_parent->radio_groups,
			      (gpointer) temp, compare_strings);

    if (list == NULL) {
	top_parent->radio_groups =
	    g_list_append(top_parent->radio_groups, temp);
    } else {
	g_free(temp);
    }
}

void
g2c_widget_add_option_menu(g2cWidget * widget)
{
    /* The option menu widget will have a property named "items", which will be a \n delimited
     * list of options.  Each one of these options needs a menu and menuitem attached to it.
     * Since glade does not do this for us, we get to do it ourselves.  
     */

    gchar **items = NULL;
    gchar *item = NULL;
    gint i = 0;
    g2cWidget *menuitem = NULL;
    g2cWidget *menu = NULL;
    gchar *widget_name = NULL;

    g_assert(widget != NULL);

    /* Step one: create a menu for this option menu */
    menu = g2c_widget_new("GtkMenu");
    widget_name = g_strdup_printf("%s_menu", widget->name);
    g2c_widget_set_property(menu, "name", widget_name);
    g2c_widget_add_subwidget(widget, menu);

    g_free(widget_name);

    /* Step two: parse the items */
    items = g_strsplit(g2c_widget_get_property(widget, "items"), "\n", 0);

    if (items != NULL) {
	while (items[i] != NULL) {
	    item = items[i];

	    /* Create a new menu for this widget */
	    menuitem = g2c_widget_new("GtkMenuItem");
	    widget_name = g_strdup_printf("%s_menuitem%d", widget->name, i);
	    g2c_widget_set_property(menuitem, "name", widget_name);
	    g2c_widget_set_property(menuitem, "label", item);
	    g2c_widget_add_subwidget(menu, menuitem);

	    g_free(widget_name);

	    i++;
	}

	g_strfreev(items);
    }
}

void
g2c_widget_create_signal_connect_cb(gpointer data, gpointer user_data)
{
    /* data      = signal
     * user_data = widget
     *
     * Each signal connected to the widget will produce a line of code
     * in the source file.  For now this function produces generic
     * calls to connect the signal, so the arguments will be arg1, arg2, etc.
     *
     * Future versions will have lookup tables to get the exact arg names.
     * For now, though, this is a quick and easy way to handle all possible
     * signals.
     */

    g2cSignal *signal = (g2cSignal *) data;
    g2cWidget *widget = (g2cWidget *) user_data;

    /* Should look like this:
     *   g_signal_connect( GTK_OBJECT( gui->widget_name ), "signal-name",
     *                     GTK_SIGNAL_FUNC( handler ),
     *                     NULL );
     */
    if (signal->object == NULL) {
	fprintf(CURRENT_FILE,
		"\tg_signal_connect%s (gui->%s, \"%s\",\n",
		signal->after ? "_after" : "", widget->name,
		signal->name);
    } else {
	fprintf(CURRENT_FILE,
		"\tg_signal_connect_object (gui->%s, \"%s\",\n",
		widget->name, signal->name);
    }

    fprintf(CURRENT_FILE, "\t                    GTK_SIGNAL_FUNC (%s),\n",
	    signal->handler);

    /* If signal->object is present, use it,
     * else you signal->data, else it's NULL */
    if (signal->object != NULL) {
	fprintf(CURRENT_FILE,
		"\t                    gui->%s, G_CONNECT_SWAPPED",
		signal->object);
	if (signal->after)
	    fprintf(CURRENT_FILE, " | G_CONNECT_AFTER");
	fprintf(CURRENT_FILE, ");\n\n");
    } else if (signal->data != NULL) {
	fprintf(CURRENT_FILE, "\t                    (gpointer) %s);\n\n",
		signal->data);
    } else {
	fprintf(CURRENT_FILE, "\t                    %s);\n\n", "NULL");
    }
}

void
g2c_widget_create_signal_prototype_cb(gpointer data, gpointer user_data)
{
    /* data      = signal
     * user_data = widget
     *
     * Each signal connected to the widget will produce a line of code
     * in the source file.  For now this function produces generic
     * calls to connect the signal, so the arguments will be arg1, arg2, etc.
     *
     * Future versions will have lookup tables to get the exact arg names.
     * For now, though, this is a quick and easy way to handle all possible
     * signals.
     * 
     * Since the user may connect several widget signals to one handler,
     * we need to make sure that there are no duplicates in the .c/.h files.
     * We DO need to connect all of the handlers, though!
     */

    g2cSignal *signal = (g2cSignal *) data;
    g2cWidget *widget = (g2cWidget *) user_data;

    GSignalQuery signal_query;
    GType signal_type = 0;
    guint signal_id = 0;
    guint i = 0;
    guint arg_name_index = 0;
    gboolean has_extra_args = FALSE;
    GType arg_name_type;

    /* if the handler name starts with gtk_, don't add */
    if (!strncmp(signal->handler, "gtk_", 4))
	return;

    /* Make sure we haven't already added this function */
    if (is_in_signal_list(signal->handler))
	return;
    else
	add_to_signal_list(signal->handler);

    signal_type = widget->klass;
    signal_id = g_signal_lookup(signal->name, signal_type);
    if (!signal_id) {
	g_print("*** UNKNOWN SIGNAL HANDLER: %s, %s\n", widget->klass_name,
		signal->name);
	exit(1);
    }

    g_signal_query(signal_id, &signal_query);
    while (signal_param_names[arg_name_index][0] != NULL) {
	arg_name_type =
	    g_type_from_name(signal_param_names[arg_name_index][0]);

	if ((g_type_is_a(widget->klass, arg_name_type)) &&
	    (strcmp(signal->name, signal_param_names[arg_name_index][1]) ==
	     0)) {
	    has_extra_args = TRUE;
	    break;
	}
	arg_name_index++;
    }

    /* Should look like this:
     *   return_code 
     *   handler( WidgetType *widget_name,
     *            ArgType arg1,
     *            ArgType argN,
     *            gpointer user_data );
     */
    fprintf(CURRENT_FILE, "%s%s\n",
	    g_type_name(signal_query.return_type),
	    G_TYPE_FUNDAMENTAL(signal_query.return_type) ==
	    G_TYPE_OBJECT ? "*" : "");

    fprintf(CURRENT_FILE, "%s (GtkWidget* widget,\n", signal->handler);

    /*
       fprintf( CURRENT_FILE, "%s (%s%s %s,\n",
       signal->handler,
       widget->klass_name,
       G_TYPE_FUNDAMENTAL( widget->klass ) == G_TYPE_OBJECT ? "*" : "", 
       widget->name );
     */

    /* Print each of the args for the signal */
    for (i = 0; i < signal_query.n_params; i++) {
	if (has_extra_args) {
	    fprintf(CURRENT_FILE, "\t%-25s %s,\n",
		    g_type_name(signal_query.param_types[i]),
		    signal_param_names[arg_name_index][i + 2]);
	} else {
	    /* <tab>argtype argN, */
	    fprintf(CURRENT_FILE, "\t%-25s arg%d,\n",
		    g_type_name(signal_query.param_types[i]), i);
	}
    }
    fprintf(CURRENT_FILE, "\t%-25s user_data);\n\n", "gpointer");
}

void
g2c_widget_create_signal_handler_cb(gpointer data, gpointer user_data)
{
    /* data      = signal
     * user_data = widget
     *
     * Each signal connected to the widget will produce a line of code
     * in the source file.  For now this function produces generic
     * calls to connect the signal, so the arguments will be arg1, arg2, etc.
     *
     * Future versions will have lookup tables to get the exact arg names.
     * For now, though, this is a quick and easy way to handle all possible
     * signals.
     *
     * Note that we only output the function if it has not been written yet!
     * Check the parser to see if this function is already implemented.
     *
     */

    g2cSignal *signal = (g2cSignal *) data;
    g2cWidget *widget = (g2cWidget *) user_data;

    g2cWidget *window = NULL;
    gchar *window_class = NULL;

    GSignalQuery signal_query;
    GType signal_type = 0;
    guint signal_id = 0;
    guint i = 0;
    guint arg_name_index = 0;
    gboolean has_extra_args = FALSE;
    GType arg_name_type;

    guint file_len = 0;
    guint cur_pos = 0;
    gchar *end_text = NULL;


    /* if the handler name starts with gtk_, don't add */
    if (!strncmp(signal->handler, "gtk_", 4))
	return;

    /* Make sure we haven't already added this function */
    if (is_in_signal_list(signal->handler))
	return;
    else
	add_to_signal_list(signal->handler);

    window = g2c_widget_get_top_parent(widget);
    window_class = g2c_transform_name(window->name, NT_TYPENAME);

    if (NULL != CURRENT_SOURCE_PARSER) {
	if (g2c_file_parser_item_exists
	    (CURRENT_SOURCE_PARSER, signal->handler))
	    goto finish;
    }

    /* There is a function to write, so open the file if not already open */
    if (NULL == CURRENT_FILE) {
	g_print("Appending to %s\n", CURRENT_FILE_NAME);

	CURRENT_FILE = fopen(CURRENT_FILE_NAME, "r+");

	/* Seek to the last brace */
	file_len = position_after_last_brace(CURRENT_FILE);
	cur_pos = ftell(CURRENT_FILE);

	/* Copy from the current location to the end of the file before writing */
	end_text = (gchar *) g_new0(gchar *, file_len - cur_pos + 2);
	fgets(end_text, file_len - cur_pos + 1, CURRENT_FILE);
	end_text = g_strchomp(end_text);

	/* Add a blank line before adding this handler */
	fprintf(CURRENT_FILE, "\n");
    }

    arg_name_index = 0;
    while (NULL != signal_param_names[arg_name_index][0]) {
	arg_name_type =
	    g_type_from_name(signal_param_names[arg_name_index][0]);

	if ((g_type_is_a(widget->klass, arg_name_type)) &&
	    (strcmp(signal->name, signal_param_names[arg_name_index][1]) ==
	     0)) {
	    has_extra_args = TRUE;
	    break;
	}
	arg_name_index++;
    }

    signal_type = widget->klass;
    signal_id = g_signal_lookup(signal->name, signal_type);
    g_assert(signal_id != 0);
    g_signal_query(signal_id, &signal_query);

    /* Should look like this:
     *   return_code 
     *   handler( WidgetType* widget_name,
     *            ArgType arg1,
     *            ArgType argN,
     *            gpointer user_data )
     *   {
     *     Window2 *window2 = (Window2*) gtk_object_get_data( GTK_OBJECT( interrupt_button ), "owner" ); 
     *   }
     */
    fprintf(CURRENT_FILE, "%s%s\n",
	    g_type_name(signal_query.return_type),
	    G_TYPE_FUNDAMENTAL(signal_query.return_type) ==
	    G_TYPE_OBJECT ? "*" : "");

    fprintf(CURRENT_FILE, "%s (GtkWidget* widget,\n", signal->handler);

    /*
       fprintf( CURRENT_FILE, "%s (%s %s%s,\n",
       signal->handler,
       widget->klass_name,
       G_TYPE_FUNDAMENTAL( widget->klass ) == G_TYPE_OBJECT ? "*" : "",
       widget->name );
     */

    /* Print each of the args for the signal */
    for (i = 0; i < signal_query.n_params; i++) {
	if (has_extra_args) {
	    fprintf(CURRENT_FILE, "\t%-25s %s,\n",
		    g_type_name(signal_query.param_types[i]),
		    signal_param_names[arg_name_index][i + 2]);
	} else {
	    /* <tab>argtype argN, */
	    fprintf(CURRENT_FILE, "\t%-25s arg%d,\n",
		    g_type_name(signal_query.param_types[i]), i);
	}
    }

    fprintf(CURRENT_FILE, "\t%-25s user_data)\n" "{\n" "\t/* %s *%s = (%s*) gtk_object_get_data (GTK_OBJECT (%s), \"owner\"); */\n" "\n" "}\n\n", "gpointer", window_class,	/* Window2 */
	    window->name,	/* window2 */
	    window_class,	/* Window2 */
	    widget->name	/* interrupt_button */
	);

    /* Copy the end text back to the end of the file */
    if (end_text != NULL) {
	fprintf(CURRENT_FILE, end_text);
    }

  finish:

    g_free(window_class);
    g_free(end_text);
}

void
g2c_widget_create_struct_cb(gpointer data, gpointer user_data)
{
    /* data      = gchar     *name
     * user_data = g2cWidget *widget
     *
     * Print out the type and name of this widget
     * <tab> type name;
     *
     */

    /*  gchar      *name   = (gchar *) data; */
    g2cWidget *widget = (g2cWidget *) user_data;

    fprintf(CURRENT_FILE,
	    "\t%s %s%s;\n", widget->klass_name, "*", widget->name);
}

void
g2c_widget_create_temp_declaration_cb(gpointer data, gpointer user_data)
{
    /* data      = g2cWidget  *widget
     * user_data = unused
     *
     * Print out temporary variables 
     * <tab> guint focus_key_name = 0;
     *
     */

#ifdef USE_GNOME
    gchar *title_color = NULL;
    gchar *text_color = NULL;
    gchar *background_color = NULL;
    gchar *logo_background_color = NULL;
    gchar *textbox_color = NULL;
#endif

    g2cWidget *widget = (g2cWidget *) data;

    g_assert(widget != NULL);

    /* If this widget is a GtkCombo subclass, we need a GList for the list elements */
    if (strcmp(widget->klass_name, "GtkCombo") == 0) {
	fprintf(CURRENT_FILE, "\tGList *%s_items = NULL;\n", widget->name);
    }
#ifdef USE_GNOME
    else if (g_type_is_a(widget->klass, GNOME_TYPE_DRUID_PAGE)) {
	/* Need temp variables to hold the colors for the druid pages
	 * Due to some funkiness in Glade, colors are saved to the XML file
	 * using color values like (255,255,255), but to make a GdkColor we
	 * need values like (65535,65535,65535).  Basically the formula is
	 * correct_color = old_color*257.  But we get the colors as a group
	 * in the for "255,255,255", so we need to parse the values first, 
	 * then multiply them by 257, then put them back together again. 
	 */
	title_color = g2c_widget_get_property(widget, "title_color");
	text_color = g2c_widget_get_property(widget, "text_color");
	background_color =
	    g2c_widget_get_property(widget, "background_color");
	logo_background_color =
	    g2c_widget_get_property(widget, "logo_background_color");
	textbox_color = g2c_widget_get_property(widget, "textbox_color");

	if (title_color)
	    fprintf(CURRENT_FILE,
		    "\tGdkColor %s_title_color = { 0,%s };\n",
		    widget->name, process_color(title_color));

	if (text_color)
	    fprintf(CURRENT_FILE,
		    "\tGdkColor %s_text_color = { 0,%s };\n",
		    widget->name, process_color(text_color));

	if (background_color)
	    fprintf(CURRENT_FILE,
		    "\tGdkColor %s_bg_color = { 0,%s };\n",
		    widget->name, process_color(background_color));

	if (logo_background_color)
	    fprintf(CURRENT_FILE,
		    "\tGdkColor %s_logo_bg_color = { 0,%s };\n",
		    widget->name, process_color(logo_background_color));

	if (textbox_color)
	    fprintf(CURRENT_FILE,
		    "\tGdkColor %s_textbox_color = { 0,%s };\n",
		    widget->name, process_color(textbox_color));
    }
#endif				/* USE_GNOME */
}

void
g2c_widget_create_accel_declaration_cb(gpointer data, gpointer user_data)
{
    /* data      = g2cWidget  *widget
     * user_data = unused
     *
     * Print out the variables to hold the accelerator keys
     * <tab> guint focus_key_name = 0;
     *
     */

    g2cWidget *widget = (g2cWidget *) data;

    g_assert(widget != NULL);

    /* If we are in gnome mode and this is a menu item, skip it */
    if (CURRENT_PROJECT->gnome_support &&
	g_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM))
	return;

    /* Handle button/label accelerators */

    if (widget->parent != NULL) {
	if (widget->parent->parent != NULL) {
	    if (g_type_is_a(widget->parent->parent->klass,
			    GTK_TYPE_OPTION_MENU))
		return;
	}
    }

    if (g_type_is_a(widget->klass, GTK_TYPE_BUTTON) &&
	!g_type_is_a(widget->klass, GTK_TYPE_OPTION_MENU)) {
	fprintf(CURRENT_FILE, "\tguint %s_key = 0;\n", widget->name);
    } else if (g_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM)) {
	if (g2c_widget_get_property(widget, "label") != NULL) {
	    fprintf(CURRENT_FILE, "\tguint %s_key = 0;\n", widget->name);
	}
    } else if (g_type_is_a(widget->klass, GTK_TYPE_LABEL) &&
	       widget->internal_child != NULL) {
	fprintf(CURRENT_FILE, "\tguint %s_key = 0;\n", widget->name);
    }
}

void
g2c_widget_create_focus_accel_declaration_cb(gpointer data,
					     gpointer user_data)
{
    /* data      = g2cAccel  *accel
     * user_data = g2cWidget *widget
     *
     * Print out the variables to hold the accelerator keys
     * <tab> guint focus_key_name;
     *
     */

    g2cAccel *accel = (g2cAccel *) data;
    g2cWidget *widget = (g2cWidget *) user_data;

    g_assert(accel != NULL);
    g_assert(widget != NULL);

    /* If we are in gnome mode and this is a menu item, skip it */
    if (CURRENT_PROJECT->gnome_support &&
	gtk_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM))
	return;

    /* Handle focus targets */
    if (accel->focus_target != NULL) {
	fprintf(CURRENT_FILE, "\tguint %s = 0;\n", accel->key);
    }
}

void
g2c_widget_create_accel_cb(gpointer data, gpointer user_data)
{
    /* data      = g2cAccel  *accel
     * user_data = g2cWidget *widget
     *
     * Print out the accelerators for this widget
     * <tab> gtk_widget_add_accelerator (close_button, "released", accel_group,
     *                          GDK_b, GDK_CONTROL_MASK | GDK_SHIFT_MASK,
     *                          GTK_ACCEL_VISIBLE);
     *
     */

    g2cAccel *accel = (g2cAccel *) data;
    g2cWidget *widget = (g2cWidget *) user_data;
    g2cWidget *target_widget = NULL;
    gchar *accel_group = NULL;

    g_assert(accel != NULL);
    g_assert(widget != NULL);

    /* If we are in gnome mode and this is a menu item, skip it */
    if (CURRENT_PROJECT->gnome_support &&
	gtk_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM))
	return;

    /* Is this a focus_target accelerator? */
    if (accel->focus_target != NULL) {
	/* Get the corresponding widget */
	target_widget =
	    g2c_widget_find_by_name(g2c_widget_get_top_parent(widget),
				    accel->focus_target);

	g2c_accel_set_signal(accel, "grab_focus");
    }

    /* Which accel group does this belong to? */
    if (gtk_type_is_a(widget->klass, GTK_TYPE_MENU_ITEM)) {
	accel_group =
	    g_strdup_printf("gui->%s_accel_group", widget->parent->name);
    } else {
	accel_group = g_strdup("gui->accel_group");
    }

    fprintf(CURRENT_FILE,
	    "\tgtk_widget_add_accelerator (GTK_WIDGET (gui->%s),\n"
	    "\t\t\"%s\",\n"
	    "\t\t%s,\n"
	    "\t\t%s,\n"
	    "\t\t%s,\n"
	    "\t\t%s);\n\n",
	    (NULL !=
	     accel->focus_target) ? accel->focus_target : widget->name,
	    accel->signal, accel_group, accel->key, accel->modifiers,
	    "GTK_ACCEL_VISIBLE");

    g_free(accel_group);
}

void
g2c_widget_create_arg_cb(gpointer key, gpointer data, gpointer user_data)
{
    /* key       = name
     * data      = value
     * user_data = widget
     */

    g2cWidget *widget = (g2cWidget *) user_data;
    gchar *name = NULL;
    gchar *value = (gchar *) data;
    gchar *type_name = NULL;
    gchar *temp_value = NULL;
    GObjectClass *oclass;
    GParamSpec *pspec;

    /* Should we worry about this property? */
    if (g2c_widget_ignore_param(widget, (gchar *) key))
	return;

    /* Is there a special handler for this property? */
    if (g2c_widget_special_handler(widget, (gchar *) key))
	return;

    /* Is this type remapped? */
    name = g2c_widget_remap_param(widget, (gchar *) key);

    oclass = g_type_class_ref(widget->klass);

    pspec = g_object_class_find_property(oclass, name);
    if (pspec != NULL) {	/* all went ok */
	switch (G_TYPE_FUNDAMENTAL(pspec->value_type)) {
	case G_TYPE_BOOLEAN:
	    type_name = g_strdup("gboolean");
	    temp_value = g2c_get_bool_s(value);
	    break;
	case G_TYPE_STRING:
	    /* Surround this thing by quotation marks, and make the gype (gchar *) */
	    type_name = g_strdup("gchar *");
	    temp_value = g2c_stringify(value);
	    break;
	default:
	    type_name = g_strdup(g_type_name(pspec->value_type));
	    temp_value = g_strdup(value);
	}

	fprintf(CURRENT_FILE,
		"\tg_object_set (gui->%s, \"%s\", (%s%s) %s, NULL);\n",
		widget->name, name, type_name,
		(G_TYPE_FUNDAMENTAL((pspec->value_type) == G_TYPE_POINTER)
		 || G_TYPE_FUNDAMENTAL((pspec->value_type) ==
				       G_TYPE_OBJECT)) ? "*" : "",
		temp_value);
    } else {
	fprintf(CURRENT_FILE,
		"*** g2c_widget_create_arg_cb: param %s::%s not found\n",
		g_type_name(widget->klass), name);
    }

    g_type_class_unref(oclass);

    g_free(name);
    g_free(type_name);
    g_free(temp_value);
}

gchar *
g2c_widget_create_string(g2cWidget * widget)
{
    /* The create string for each widget may be a little different.
     * Under most circumstances, it is simply <widget_class>_new().
     * We keep a list of alternate functions, so check there first.
     * If not listed in the alternate functions list, just return the
     * standard <widget_class>_new().
     */

    int i = 0;
    int j = 0;
    int k = 0;
    gchar *result = NULL;
    gchar *str = NULL;
    gchar *temp_param = NULL;
    gchar *params[MAX_CREATE_PARAMS];
    gchar *param_str = NULL;
    gchar *func_str = NULL;
    gchar *trans_name = g2c_transform_name(widget->klass_name, NT_FUNCTION);

    for (k = 0; k < MAX_CREATE_PARAMS; k++)
	params[k] = NULL;

    /* Look up in table */
    while (create_functions[i].widget_class != NULL) {
	if (!strcmp(widget->klass_name, create_functions[i].widget_class)) {
	    /* Is there a special function for this class? */
	    if (create_functions[i].handler != NULL) {
		(*(create_functions[i].handler)) (widget);

		g_free(trans_name);

		return NULL;
	    }

	    /* Get the parameters values */
	    while (create_functions[i].parameters[j] != NULL) {
		g_assert(j <= MAX_CREATE_PARAMS);

		temp_param = g_strdup(g2c_widget_get_property(widget,
							      create_functions
							      [i].
							      parameters
							      [j]));

		/* If this is a string value, quote it */
		params[j] = g2c_format_argument(widget->klass_name,
						create_functions[i].
						parameters[j], temp_param);

		g_free(temp_param);

		j++;
	    }
	    params[j] = NULL;

	    /* Can't think of a better way.  Suggestions wilkommen. */
	    func_str = g_strdup_printf(create_functions[i].create_function,
				       params[0],
				       params[1],
				       params[2],
				       params[3],
				       params[4],
				       params[5],
				       params[6],
				       params[7], params[8], params[9]
		);

	    str = g_strdup_printf("\tgui->%s = (%s*) %s;\n",
				  widget->name,
				  widget->klass_name, func_str);

	    g_free(func_str);
	    g_free(param_str);

	    for (i = 0; i < MAX_CREATE_PARAMS; i++)
		g_free(params[i]);

	    g_free(trans_name);

	    return str;
	}

	i++;
    }

    /* Return the standard create function */
    result = g_strdup_printf("\tgui->%s = (%s*) %s_new ();\n",
			     widget->name, widget->klass_name, trans_name);
    g_free(trans_name);

    return result;
}

g2cWidget *
g2c_widget_find_by_name(g2cWidget * widget, const gchar * widget_name)
{
    GList *child = NULL;
    g2cWidget *target = NULL;

    g_assert(widget != NULL);
    g_assert(widget_name != NULL);

    if (strcmp(widget->name, widget_name) == 0)
	return widget;

    child = g_list_first(widget->children);

    while (child != NULL) {
	target =
	    g2c_widget_find_by_name((g2cWidget *) child->data,
				    widget_name);
	if (target != NULL)
	    break;

	child = g_list_next(child);
    }

    return target;
}

g2cWidget *
g2c_widget_find_by_type_name(g2cWidget * widget, const gchar * type_name)
{
    GList *child = NULL;
    g2cWidget *target = NULL;

    g_assert(widget != NULL);
    g_assert(type_name != NULL);

    if (strcmp(widget->klass_name, type_name) == 0)
	return widget;

    child = g_list_first(widget->children);

    while (child != NULL) {
	target =
	    g2c_widget_find_by_type_name((g2cWidget *) child->data,
					 type_name);
	if (target != NULL)
	    break;

	child = g_list_next(child);
    }

    return target;
}

g2cWidget *
g2c_widget_find_by_type(g2cWidget * widget, GType type)
{
    /* Finds the first child widget of the given type */
    GList *child = NULL;
    g2cWidget *target = NULL;

    g_assert(widget != NULL);

    if (gtk_type_is_a(widget->klass, type))
	return widget;

    child = g_list_first(widget->children);

    while (child != NULL) {
	target = g2c_widget_find_by_type((g2cWidget *) child->data, type);
	if (target != NULL)
	    break;

	child = g_list_next(child);
    }

    return target;
}

g2cWidget *
g2c_widget_find_non_internal_parent(g2cWidget *widget)
{
    while (widget && widget->internal_child != NULL)
	widget = widget->parent;
    return widget;
}

void
g2c_widget_output_after_children_cb(gpointer key,
				    gpointer data, gpointer user_data)
{
    gchar *param = (gchar *) key;
    gchar *value = (gchar *) data;
    g2cWidget *widget = (g2cWidget *) user_data;

    int i = 0;
    int j = 0;
    gchar *params[MAX_CREATE_PARAMS];
    GType param_type;

    g_assert(param != NULL);

    // g_assert( NULL != value );
    if (value == NULL) {
	g_print("NULL value for %s in %s\n", param, widget->name);
	value = "";
    }

    g_assert(widget != NULL);

    /* If this parameter is in the list of parameters in after_params,
     * then either output a formatted statement or call a handler function
     */

    while (after_params[i].param != NULL) {
	param_type = g_type_from_name(after_params[i].type);
	if (gtk_type_is_a(widget->klass, param_type) ||
	    !strcmp(widget->klass_name, after_params[i].type)) {
	    if (!strcmp(after_params[i].param, param)) {
		/* Does this handler have a special function to call? */
		if (after_params[i].handler != NULL) {
		    (*(after_params[i].handler)) (widget);

		    return;	/* It was handled! */
		}

		/* Use the format function */
		for (j = 0; j < MAX_CREATE_PARAMS; j++)
		    params[j] =
			g2c_get_bool_s(g2c_widget_get_property
				       (widget,
					after_params[i].params[j]));

		/* Can't think of a better way.  Suggestions wilkommen. */
		fprintf(CURRENT_FILE,
			after_params[i].format,
			params[0],
			params[1], params[2], params[3], params[4]
		    );

		for (j = 0; j < MAX_CREATE_PARAMS; j++)
		    g_free(params[j]);
	    }
	}
	i++;
    }
}

gboolean
g2c_widget_ignore_param(g2cWidget * widget, gchar * param)
{
    int i = 0;
    GType param_type;

    while (ignore_params[i].type != NULL) {
	param_type = g_type_from_name(ignore_params[i].type);
	if (g_type_is_a(widget->klass, param_type) ||
	    strcmp(widget->klass_name, ignore_params[i].type) == 0) {
	    if (strcmp(ignore_params[i].param, param) == 0)
		return TRUE;
	}
	i++;
    }

    i = 0;

    while (after_params[i].type != NULL) {
	param_type = g_type_from_name(after_params[i].type);
	if (g_type_is_a(widget->klass, param_type) ||
	    strcmp(widget->klass_name, after_params[i].type) == 0) {
	    if (strcmp(after_params[i].param, param) == 0)
		return TRUE;
	}
	i++;
    }

    return FALSE;
}

gchar *
g2c_widget_remap_param(g2cWidget * widget, gchar * param)
{
    int i = 0;
    GType param_type;

    while (remap_params[i].type != NULL) {
	param_type = g_type_from_name(remap_params[i].type);
	if (g_type_is_a(widget->klass, param_type) ||
	    strcmp(widget->klass_name, ignore_params[i].type) == 0) {
	    if (strcmp(remap_params[i].old_param, param) == 0)
		return g_strdup(remap_params[i].new_param);
	}
	i++;
    }

    return g_strdup(param);
}

gboolean
g2c_widget_special_handler(g2cWidget * widget, gchar * keyword)
{
    int i = 0;
    int j = 0;
    gchar *params[MAX_CREATE_PARAMS];
    GType param_type;

    while (special_handlers[i].keyword != NULL) {
	param_type = g_type_from_name(special_handlers[i].type);

	if (g_type_is_a(widget->klass, param_type) ||
	    strcmp(widget->klass_name, special_handlers[i].type) == 0) {
	    if (strcmp(special_handlers[i].keyword, keyword) == 0) {
		/* Does this handler have a special function to call? */
		if (special_handlers[i].handler != NULL) {
		    (*(special_handlers[i].handler)) (widget);

		    return TRUE;	/* It was handled! */
		}
		/* Does this handler have a conditional use? */
		if (special_handlers[i].conditional != NULL) {
		    if (!g2c_get_bool(g2c_widget_get_property(widget,
							      special_handlers
							      [i].
							      conditional)))
			return TRUE;	/* It was handled! */
		}

		for (j = 0; j < MAX_CREATE_PARAMS; j++)
		    params[j] = NULL;

		for (j = 0; j < MAX_CREATE_PARAMS; j++) {
		    /* Handle null fields */
		    if (special_handlers[i].params[j] == NULL) {
			params[j] = NULL;
		    }
		    /* Handle string values */
		    else if (!strncmp(special_handlers[i].params[j], "$", 1)) {
			params[j] =
			    g2c_stringify(g2c_widget_get_property
					  (widget,
					   special_handlers[i].params[j] +
					   1));
		    }
		    /* Handle NULL values */
		    else if (g2c_widget_get_property(widget,
						     special_handlers[i].
						     params[j]) == NULL) {
			params[j] = NULL;
		    }
		    /* Handle boolean values */
		    else {
			params[j] =
			    g2c_get_bool_s(g2c_widget_get_property
					   (widget,
					    special_handlers[i].
					    params[j]));
		    }
		}

		/* Can't think of a better way.  Suggestions wilkommen. */
		fprintf(CURRENT_FILE,
			special_handlers[i].format,
			params[0],
			params[1],
			params[2],
			params[3],
			params[4],
			params[5],
			params[6], params[7], params[8], params[9]
		    );

		for (j = 0; j < MAX_CREATE_PARAMS; j++)
		    g_free(params[j]);

		return TRUE;
	    }
	}
	i++;
    }

    return FALSE;
}

guint
position_after_last_brace(FILE * file)
{
    /* Find the last brace in the file */
    long pos = 0;
    int rc = 0;
    guint len = 0;

    rc = fseek(file, pos, SEEK_END);
    g_assert(rc == 0);

    len = ftell(file);

    while (TRUE) {
	pos--;

	rc = fseek(file, pos, SEEK_END);
	if (rc != 0) {
	    if (0 != ftell(file)) {
		/* No functions found.  Odd.  Place at end of file */
		fseek(file, 0, SEEK_END);

		return 0;
	    } else {
		g_print("fseek returned %d when pos=%ld\n", rc, pos);
		g_assert_not_reached();
	    }
	}

	/* Test the current character to see if it is a }.
	 * If so, then we just read past it, so the file location
	 * is right after the brace
	 */
	rc = fgetc(file);
	if (rc == '}')
	    break;
    }

    return len;
}

gchar *
process_color(gchar * color)
{
    /* Take a string of format "255,255,255" and multiply each of the
     * values by 257, returning a string like "65535,65535,65535"
     */
    gchar **colors = NULL;
    gchar *result = NULL;
    gchar *red, *green, *blue;
    int color_value;

    colors = g_strsplit(color, ",", -1);

    color_value = atoi(colors[0]) * 257;
    red = g_strdup_printf("%d", color_value);

    color_value = atoi(colors[1]) * 257;
    green = g_strdup_printf("%d", color_value);

    color_value = atoi(colors[2]) * 257;
    blue = g_strdup_printf("%d", color_value);

    result = g_strdup(g_strjoin(",", red, green, blue, NULL));

    g_free(red);
    g_free(green);
    g_free(blue);

    g_strfreev(colors);

    return result;
}
