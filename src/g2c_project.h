/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef G2C_PROJECT_H
#define G2C_PROJECT_H

#include <glib.h>
#include "g2c_widget.h"

typedef struct tag_g2cProject {
    GHashTable *properties;
    GList *top_level_widgets;
    gchar *name;
    gchar *program_name;
    gchar *directory;
    gchar *source_directory;
    gchar *pixmaps_directory;
    gboolean gettext_support;
    gboolean gnome_support;
    gboolean gnome_help_support;
    gboolean output_main_file;
    gboolean output_support_files;
    gboolean output_build_files;
    gboolean has_bonobo_controls;
} g2cProject;

g2cProject *CURRENT_PROJECT;

g2cProject *g2c_project_new(void);

void g2c_project_destroy(g2cProject * project);

void  g2c_project_set_property(g2cProject * project, const gchar * name,
			       const gchar * value);

const gchar *g2c_project_get_property(g2cProject * project,
				      const gchar * name);

void g2c_project_add_top_level_widget(g2cProject * project,
				      g2cWidget * widget);


#endif
